@extends('main')
@section('contents')
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            <h1>Tags</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tags as $tag)
                    <tr>
                        <th>{{ $tag->id }}</th>
                        <th><a href="{{ route('tags.show', $tag->id) }}">{{ $tag->name }}</a></th>
                        <th>
                            <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-primary btn-sm" style="float: left;"> Edit </a>
                             {!! Form::open(['route'=>['tags.destroy',$tag->id],'method'=>'DELETE']) !!}
                                {{ Form::submit('Delete',['class'=>'btn btn-danger btn-sm']) }}
                            {!! Form::close() !!}
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-3 col-md-offset-1 form-spacing-top">
            <div class="well">
                {!! Form::open(['route' => 'tags.store', 'method' => 'POST']) !!}
                <h2>New Tag</h2>
                {{ Form::label('name', 'Name:') }}
                {{ Form::text('name', null, ['class' => 'form-control']) }}

                {{ Form::submit('Create New Tag', ['class' => 'btn btn-primary btn-block btn-h1-spacing']) }}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
        
        {{-- Modal-edit --}}
    <div id="myModal-2" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
            <p style="display: none;" id="edit">{{ $edit }}</p>
                <div class="modal-header ">
                    <span>Edit</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div class="row">
                        <div class="col-md-12">
                            {{ Form::model($tagedit, ['route' => ['tags.update', $tagedit->id], 'method' => "PUT"]) }}

                            {{ Form::label('name', "Tên:") }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}

                            {{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}

                            {{ Form::close() }}
                           </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end-modal-edit --}}
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        console.log($('#edit').text().length);
        if($('#edit').text().length>2){
            $("#myModal-2").modal('show');
        }
    });
</script>
@stop