<?php

namespace App\Http\Controllers\Comments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Comments;
use App\products;
use App\User;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // dd($id);
        $comment = new Comments;
        $product = products::find($id);
        $user = Auth::User();

        $comment->name = $user->name;
        $comment->email = $user->email;
        $comment->comment=$request->comments;
        $comment->star = $request->chamsao;

        $danhgia = array();
        for($i=0;$i<$request->chamsao;$i++)
        {
            $danhgia[$i] = 100;
        }
        if($request->chamsao<5)
        {
             for($i=$request->chamsao;$i<5;$i++)
                {
                    $danhgia[$i] = 0;
                }
        }
        $danh_gia_star = collect($danhgia);
        $comment->display_star = $danh_gia_star;

        $comment->products()->associate($product);
        $comment->user()->associate($user);

        $comment->save();

        return redirect()->route('products.show', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
