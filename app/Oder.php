<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oder extends Model
{
    public function products(){
		return $this->belongsTo('App\products');
	}
	
	public function user(){
		return $this->belongsTo('App\User');
	}
}
