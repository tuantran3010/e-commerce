<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\products;
use App\Cart;

class TestController extends Controller
{
	public function test(){
		$categories = Categories::all()->where('name','Vòng Tay Bóng Đá')->first();
		// dd($categories);
		$products = products::all()->where('categories_id',$categories->id);
		$carts = Cart::all();
		foreach($products as $product){
			if(!is_null($product->Comments))
				{
					$product->votes = (float)$product->Comments->avg('star');
				}
			else
				{
					$product->votes=0;
				}
			$product->save();

			$int = (int) $product->votes;
			$float = $product->votes - $int;
			$danhgia = array();
			for($i=0;$i<$int;$i++){
				$danhgia[$i]=100;
			}
			$danhgia[$int]=(int)($float*100);		
			if($int<4){ 
				for($i=$int+1;$i<5;$i++){
					$danhgia[$i]=0;
				}
			}
			// dd($danhgia);
			$danh_gia_star = collect($danhgia);
			$product->stars = $danh_gia_star;
			$product->save();
			// dd($product->stars[0]);
		}

    	return view('products/productspage')->withProducts($products)->withCarts($carts);
	}  

	public function tuong(){
		$categories = Categories::all()->where('name','Tượng Bóng Đá');		
		$products = products::all()->where('categories_id',$categories[0]->id);
		// $products = products::all();
		$carts = Cart::all();
		foreach($products as $product){
			if(!is_null($product->Comments))
				{
					$product->votes = (float)$product->Comments->avg('star');
				}
			else
				{
					$product->votes=0;
				}
			$product->save();

			$int = (int) $product->votes;
			$float = $product->votes - $int;
			$danhgia = array();
			for($i=0;$i<$int;$i++){
				$danhgia[$i]=100;
			}
			$danhgia[$int]=(int)($float*100);		
			if($int<4){ 
				for($i=$int+1;$i<5;$i++){
					$danhgia[$i]=0;
				}
			}
			// dd($danhgia);
			$danh_gia_star = collect($danhgia);
			$product->stars = $danh_gia_star;
			$product->save();
			// dd($product->stars[0]);
		} 

		return view('products/productspage')->withProducts($products)->withCarts($carts);
	}
	public function video(){
		$carts = Cart::all();
		return view('test/test')->withCarts($carts);
	}  
}
