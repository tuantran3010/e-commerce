@extends('main')
@section('contents')
@section('stylesheets')
  {!! $map['js'] !!}
@endsection
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div style="text-align: center;">
					<h3 id="nameshop">Shop Tượng Bóng Đá</h3>
				</div>
				<div>
					<p class="intro-item">Tượng cầu thủ bóng đá mini chuyên cung cấp mô hình tượng cầu thủ bóng đá thương hiệu kodoto cầu thủ và mô hình sân bóng đá các đội bóng nổi tiếng trên thế giới. <br>
					Đặc biệt khi mua tượng tại shop các bạn sẽ được tham gia chương trình khuyễn mãi mua tượng tích điểm - đổi điểm lấy tượng và các phần quà có giá trị khác.</p>
					<img src="image/anhteammu.jpg" alt="" style="width: 100%;">
				</div>
				<br>
				<div>
					<p class="intro-item">Một số Mô Hình Sân, Vòng Tay Cũng Đang Được Shop Triển Khai Trên Các Trang Thương Mại Điện Tử Như Shoppe, Tiki, Sendo,... </p>
					<br>
					<img src="image/anhvongtay.jpg" alt="">
					<br>
					<img src="image/anhbongro.jpg" alt="" style="width: 100%;padding-top: 12px;padding-bottom: 12px;">
				</div>
				<p class="intro-item">Địa Chỉ Shop</p>
				<div>
					{!! $map['html'] !!}
				</div>
				<div>
					<strong style="padding-top:32px;float: right;">TUAN TRAN</strong>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div>
		@include('footer')
	</div>
@endsection