<html>
	<head>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<style>
			body{
				background: #112c38;
			}
			.social{
				margin: 0;
				padding: 0;
				height: 100vh;
				display: flex;
				align-items: center;
				justify-content: center;
			}
			.menu{
				margin: 0;
				padding: 0;
				display: flex;
				justify-content: center;
				/*align-items: center;*/
				/*height: 100vh;*/
				background: #112c38
			}
			.menu ul{
				margin: 0;
				padding: 0;
				display: flex;
				height: 34px;
				/*justify-content: center;*/
				padding: 16px;
			}
			.menu ul li{
				list-style: none;
				margin: 0 20px;
				transition: 0.5s;
			}
			.menu ul li a{
				display: block;
				position: relative;
				text-decoration: none;
				padding: 5px;
				font-family: sans-serif;
				font-size: 18px;
				color: white;
				text-transform: uppercase;
				transition: 0.5s;
				/*background: #ff497c;*/
			}
			.menu ul:hover li a{
				transform: scale(1.5);
				opacity: .2;
				filter: blur(5px);
			}
			.menu ul li a:hover{
				transform: scale(2);
				opacity: 1;
				filter: blur(0);
			}
			.menu ul li a:before{
				content: '';
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background: #ff497c;
				transition: transform 0.5s;
				transform-origin: right;
				transform: scaleX(0);
				z-index: -1;
			}
			.menu ul li a:hover:before{
				transition: transform 0.5s;
				transform-origin: left;
				transform: scaleX(1);
			}
			.social ul{
				position: relative;
				margin: 0;
				padding: 0;
				display: flex;
			}
			.social ul li{
				position: relative;
				list-style: none;
				width: 60px;
				height: 60px;
				margin: 0 30px;
				transform: rotate(-30deg) skew(25deg);
				/*background: #000;*/
			}
			.social ul li span{
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background:#000;
				transition: 0.5s;
				display: flex !important;
				align-items: center;
				justify-content: center;
				color: #fff;
				font-size: 30px !important;
			}
			.social ul li:nth-child(1) span{
				background: #dd4b39;
			}
			.social ul li:nth-child(2) span{
				background: #3b3999;
			}
			.social ul li:nth-child(3) span{
				background: #0077b5;
			}
			.social ul li:nth-child(4) span{
				background: #e4405f;
			}
			.social ul li:hover span:nth-child(5){
				transform: translate(40px,-40px);
				opacity: 1;
			}
			.social ul li:hover span:nth-child(4){
				transform: translate(30px,-30px);
				opacity: .8;
			}
			.social ul li:hover span:nth-child(3){
				transform: translate(20px,-20px);
				opacity: .6 ;
			}
			.social ul li:hover span:nth-child(2){
				transform: translate(10px,-10px);
				opacity: .4 ;
			}
			.social ul li:hover span:nth-child(1){
				transform: translate(0,0);
				opacity: .2 ;
			}
		</style>
	</head>
	<body>
		<div class="menu">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">About</a></li>
				<li><a href="">Service</a></li>
				<li><a href="">Contact</a></li>
				<li><a href="">Team</a></li>
			</ul>
		</div>
		<div class="social" style="padding-top: 32px;">
			<ul>
				<li><a href="">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span class="fab fa-youtube"></span>
					</a></li>
				<li><a href="">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span class="fab fa-facebook"></span>
					</a></li>
				<li><a href="">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span class="fab fa-twitter"></span>
					</a></li>
				<li><a href="">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span class="fab fa-instagram"></span>
					</a></li>
			</ul>
		</div>
	</body>
</html>