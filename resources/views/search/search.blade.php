@extends('main')
@section('contents')
<div >
	<div class="container" style="margin-top: 39px">
		<div class="search" style="border-bottom: solid 3px green;">
			<strong style="font-size: 30px">{{ $titles }}</strong>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="sort-bar">
					<span class="sort">Sắp Xếp Theo</span>
					<div>
						<div class="sort fix">
							<select class="form-control form-control-lg">
							  	<option>Giá</option>
							  	<option> < 100000 vnđ</option>
							  	<option> > 100000 vnđ</option>
							</select>
						</div>
						<div style="float: right;">
						    <form class="navbar-form navbar-left" action="/action_page.php" >
							     <div class="form-group">
							        <input type="text" class="form-control" placeholder="Search" id="myInput">
							     </div>
							     <button type="submit" class="btn btn-default">Tìm Kiếm</button>
							</form>	
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row" id="myTable">
			@foreach($products as $product)
				<div class="col-md-3" id="khung">
					<div class="items">
						<div class="image-item">
							<img src="{{ asset('image/'.$product->image_main) }}" alt="" class="size-image avataritem" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">
							<a href="{{ route('products.edit', $product->id) }}" id="edit"><i class="far fa-edit edit"></i></a>
						</div>
						<div class="title-item">
							<p style="cursor: pointer;"><a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a></p>
						</div>
						<div class="price-item">
							<p style="float: left;">{{ $product->price }} vnđ</p>
								@if( $product->stars[0] ==0)
									<div style="padding-left: 71px;">
										<span style="border-left: solid 1px #000;padding-left: 2px;">Chưa Có Đánh Giá</span>
									</div>
								@else
							    <div style="padding-left: 88px">
						  			<div style="display: flex;" id="star">
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[0] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[1] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>				
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[2] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[3] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[4] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
									</div>
							  		<div style="display: flex;position: relative;bottom: 16px;color:yellow;">
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
									</div>
								</div>
							@endif
						</div>
						<div class="giohang">
							<button class="btn btn-primary " style="margin-top: -3px;margin-left: 32px;" id="themhang">Thêm Vào Giỏ Hàng</button>
						</div>
					</div>
				</div>
			@endforeach	
		</div>
	</div>
	<div>
		<ul class="pagination" style="margin-left: 488px;">
		    <li><a href="#" style="background: gainsboro;">1</a></li>
		    <li><a href="#">2</a></li>
		    <li><a href="#">3</a></li>
		    <li><a href="#">4</a></li>
		    <li><a href="#">5</a></li>
		</ul>
	</div>

	<div>
		@include('footer')
	</div>
</div>
    <script>
    	$(document).ready(function(){
    	 	$("#fanpage").click(function(){
		        $("#page").show();
		    });
		 	$(".fanpage_tuong").mouseleave(function(){
		     	$("#page").hide();
		    });
		});
    </script>
    <script src='{{ asset('js/add_to_cart.js') }}' type="text/javascript"></script>
@stop