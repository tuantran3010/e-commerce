@extends('main')
@section('contents')

@section('stylesheets')
    {!!Html::style('css/select2.min.css')!!}
    {{-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ 
        selector:'textarea',
        plugins : 'link code'
      });
   </script> --}}
   <script type="text/javascript" src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
@endsection
<div style="background: #f5f5f5">
<div class="container">
	<div class="row">
    {{-- <div class="col-md-2"></div> --}}
  	<div class="col-md-8 col-md-offset-2">
  		<h1 style="float: left;">Edit Product</h1>
      <div style="position: relative;left: 485px;top: 19px;">
        {!! Form::open(['route'=>['products.destroy',$product->id],'method'=>'DELETE']) !!}
          {{ Form::submit('Delete',['class'=>'btn btn-danger btn-sm']) }}
        {!! Form::close() !!}
      </div>
  		<hr>
  		{{ Form::model($product, ['route' => ['products.update', $product->id], 'method' => "PUT",'files' => true]) }}
   			{{ Form::label('name','Tên Sản Phẩm') }}
   			{{ Form::text('name',null,array('class'=>'form-control')) }}
        @if ($errors->has('name'))
          <div class="alert alert-danger">
            <strong>{{ $errors->first('name') }}</strong>
          </div>
        @endif
   			{{ Form::label('price','Giá Sản Phẩm') }}
   			{{ Form::text('price',null,array('class'=>'form-control')) }}
				@if ($errors->has('price'))
          <div class="alert alert-danger">
            <strong>{{ $errors->first('price') }}</strong>
          </div>
        @endif
   			{{ Form::label('mieuta','Miêu Tả') }}
   			{{ Form::textarea('mieuta',null,array('class'=>'form-control')) }}
        <script type="text/javascript">
          var editor = CKEDITOR.replace('mieuta',{
           language:'vi',
           filebrowserBrowseUrl :'js/ckfinder/ckfinder.html',
           filebrowserImageBrowseUrl : 'js/ckfinder/ckfinder.html?type=Images',
           filebrowserFlashBrowseUrl : 'js/ckfinder/ckfinder.html?type=Flash',
           filebrowserUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
           filebrowserImageUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
           filebrowserFlashUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
           });
        </script>﻿
        @if ($errors->has('mieuta'))
          <div class="alert alert-danger">
            <strong>{{ $errors->first('mieuta') }}</strong>
          </div>
        @endif
   			{{ Form::label('xuatxu','Xuất Xứ Sản Phẩm') }}
   			{{ Form::text('xuatxu',null,array('class'=>'form-control')) }}
        @if ($errors->has('xuatxu'))
          <div class="alert alert-danger">
            <strong>{{ $errors->first('xuatxu') }}</strong>
          </div>
        @endif

        {{ Form::label('categories', 'Categories:') }}
        <select class="form-control" name="categories[]">
            @foreach($categories as $categori)
            	<option value="{{ $categori->id }}">{{ $categori->name }}</option>
            @endforeach
        </select>
        
        <div style="display: flex;">
          <div>
            {{ Form::label('image_main','Upload Image Main') }}
            {{ Form::file('image_main',array('class'=>'newavatar')) }}
            <img src="{{ asset('image/'.$product->image_main) }}" style="height: 220px;width: 220px;">
            @if ($errors->has('image_main'))
            <div class="alert alert-danger">
              <strong>{{ $errors->first('image_main') }}</strong>
            </div>
            @endif
          </div>
            
          <div>
            {{ Form::label('image_sub1','Upload Image Sub 1') }}
            {{ Form::file('image_sub1',array('class'=>'newavatar')) }}
            <img src="{{ asset('image/'.$product->image_sub1) }}" style="height: 220px;width: 220px;">
          </div>

          <div>
            {{ Form::label('image_sub2','Upload Image Sub 2') }}
            {{ Form::file('image_sub2',array('class'=>'newavatar')) }}
            <img src="{{ asset('image/'.$product->image_sub2) }}" style="height: 220px;width: 220px;">
          </div>
        </div>
        <br>
   			{{ Form::submit('Edit Product',array('class'=>'btn btn-success btn-lg btn-block')) }}
		  {!! Form::close() !!}
  	</div>
  </div>
</div>
</div>

@section('scripts')

  {!! Html::script('js/select2.min.js') !!}

  <script type="text/javascript">
      $('.select2-multi').select2();
      $('.select2-multi').select2().val({!!$product->tags()->allRelatedIds() !!}).trigger('change');
  </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(".newavatar").change(function(){
        console.log(this);
        let url = URL.createObjectURL(this.files[0]);
        console.log($(this).parent().find('img'));
        $($(this).parent().find('img')).attr('src', url);
        $($(this).parent().find('img')).width(220);
        $($(this).parent().find('img')).height(220);
      })
    </script>
@endsection
@stop