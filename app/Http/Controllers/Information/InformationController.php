<?php

namespace App\Http\Controllers\Information;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Cart;
use App\Tag;
use App\products;
use Image;
use GMaps;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function intro(){
        $user = Auth::User();
        $carts = $user->Carts;
        $config = array();
        $config['zoom'] = '16';
        $config['map_height'] = '400px';

        $market = array();
        GMaps::initialize($config);

        GMaps::add_marker($market);
        $map = GMaps::create_map();
        return view('intro/intro')->withUser($user)->withCarts($carts)->withMap($map);
    }

    public function index()
    {
        $user = Auth::User();
        $carts = $user->Carts;
        // dd($carts);
        return view('information/user')->withUser($user)->withCarts($carts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::User();
        $carts = $user->Carts;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->phone = $request->phone;
        if($request->hasFile('avatar'))
        {
            $image=$request->file('avatar');
            $filename=time().'.'.$image->getClientOriginalExtension();
            //dd($filename);
            $localtion= public_path('image/'.$filename);
            Image::make($image)->save($localtion);
            $user->image=$filename;
        }
        $user->save();
        $newuser = Auth::User();
        return view('information/user')->withUser($newuser)->withCarts($carts);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test(Request $request)
    {
        $cart = new Cart;
        // $tag = new Tag;
        $user = Auth::User();
        $products = products::all();
        $check = Auth::User()->carts;
        $checks = $check->where('name', $request->name)->first();
        if(!isset($checks->name)){
            $product = $products->where('name', $request->name)->first();
                $cart->name = $request->name;
                $cart->soluong = 1;
                $cart->gia = $product->price;
                $cart->tong = '100000';
                $cart->products()->associate($product);
                $cart->user()->associate($user);
                // $tag->save();
                $cart->save();
        }
        else{
            $checks->soluong = $checks->soluong+1;
            $checks->save();
        }
        // $tag->name = $request->name;
        // $cart->name = $request->name;
        // $cart->soluong = 4;
        // $cart->gia = $product->price;
        // $cart->tong = '100000';
        // $cart->products()->associate($product);
        // $cart->user()->associate($user);
        // // $tag->save();
        // $cart->save();
    }
}
