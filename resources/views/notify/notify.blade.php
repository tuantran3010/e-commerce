@extends('main')
@section('contents')
@section('stylesheets')
  {!!Html::style('css/select2.min.css')!!}
   <script type="text/javascript" src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
@endsection
  <div style="background: #f5f5f5">
    <div class="container">
    	<div class="row">
        {{-- <div class="col-md-2"></div> --}}
      	<div class="col-md-8 col-md-offset-2">
          <div>
            <h1>Create New Notify</h1>
            <div>
              <button class="btn btn-primary"><a href="{{ route('notify.index') }}" style="color: white;text-decoration: none;">Edit Notify</a></button>
            </div>
          </div>
      		<hr>
          {{-- Create form --}}
      		{!! Form::open(array('route'=>'notify.store','files' => true)) !!}
            {{-- create title --}}
       			{{ Form::label('title','Title') }}
       			{{ Form::text('title',null,array('class'=>'form-control')) }}
            @if ($errors->has('title'))
              <div class="alert alert-danger">
                <strong>{{ $errors->first('title') }}</strong>
              </div>
            @endif
            {{-- create describe --}}
       			{{ Form::label('describe','Describe') }}
       			{{ Form::textarea('describe',null,array('class'=>'form-control')) }}
            <script type="text/javascript">
              var editor = CKEDITOR.replace('describe',{
               language:'vi',
               filebrowserBrowseUrl :'js/ckfinder/ckfinder.html',
               filebrowserImageBrowseUrl : 'js/ckfinder/ckfinder.html?type=Images',
               filebrowserFlashBrowseUrl : 'js/ckfinder/ckfinder.html?type=Flash',
               filebrowserUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
               filebrowserImageUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
               filebrowserFlashUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
               });
            </script>﻿
            @if ($errors->has('describe'))
              <div class="alert alert-danger">
                <strong>{{ $errors->first('describe') }}</strong>
              </div>
            @endif
            {{-- add image to notify --}}
            {{ Form::label('image','Upload Image Main') }}
            {{ Form::file('image',array('class'=>'newavatar')) }}
              <img alt="">
              @if ($errors->has('image'))
                <div class="alert alert-danger">
                  <strong>{{ $errors->first('image') }}</strong>
                </div>
              @endif
            {{-- submit --}}
       			{{ Form::submit('Create Notify',array('class'=>'btn btn-success btn-lg btn-block','style'=>'margin-top:10px')) }}
    		  {!! Form::close() !!}
      	</div>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(".newavatar").change(function(){
      console.log(this);
      let url = URL.createObjectURL(this.files[0]);
      console.log($(this).parent().find('img'));
      $($(this).parent().find('img')).attr('src', url);
      $($(this).parent().find('img')).width(220);
      $($(this).parent().find('img')).height(220);
    })
  </script>
@endsection