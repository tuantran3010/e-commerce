@extends('main')
@section('contents')
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            <h1>Categories</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($categories as $categori)
                        <tr>
                            <th>{{ $categori->id }}</th>
                            <th><a href="{{ route('categories.show', $categori->id) }}">{{ $categori->name }}</a></th>
                            <th>
                            	<a href="{{ route('categories.edit', $categori->id) }}" class="btn btn-primary btn-sm" style="float: left;"> Edit </a>
                                {!! Form::open(['route'=>['categories.destroy',$categori->id],'method'=>'DELETE']) !!}
                                    {{ Form::submit('Delete',['class'=>'btn btn-danger btn-sm']) }}
                                {!! Form::close() !!}
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
		
		{{-- Modal-edit --}}
	<div id="myModal-2" class="modal fade" role="dialog">
	    <div class="modal-dialog">
        <p style="display: none;" id="edit">{{ $edit }}</p>
	    <!-- Modal content-->
		    <div class="modal-content">
		    	<div class="modal-header ">
				    <span>Edit</span>
				    <button type="button" class="close" data-dismiss="modal">&times;</button>
		    	</div>

		    	<div class="modal-body">
		      	    <div class="row">
					    <div class="col-md-12">
						    {{ Form::model($categorie, ['route' => ['categories.update', $categorie->id], 'method' => "PUT"]) }}

    						    {{ Form::label('name', "Tên:") }}
    						    {{ Form::text('name', null, ['class' => 'form-control']) }}
    						    {{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
                                
						    {{ Form::close() }}
						</div>
					</div>
				</div>
    		</div>
    	</div>
    </div>
	{{-- end-modal-edit --}}
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
    	$(document).ready(function(){
            console.log($('#edit').text().length);
            console.log('abc');
    		if($('#edit').text().length>2){
    			$("#myModal-2").modal('show');
    		}
    	});
    </script>
@stop