<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/parsley.css') }}">
	@yield('stylesheets')
</head>

<body>
	<div class="drop" style="width: 100%">
		<ul class="menu" style="margin-bottom: 0px;">
			<li id="myhome" ><i class="fas fa-shopping-cart"></i> <a href="http://localhost/E-commerce/public/home">Shop-A</a></li>
			<li><a href="#">Tượng Cầu Thủ</a>
				<ul>
					<li><a href="#">Chelsea</a></li>
					<li><a href="#">Manu</a>
						<ul>
							<li><a href="#">Ronaldo</a></li>
							<li><a href="#">Rooney</a></li>
							<li><a href="#">Lukaku</a></li>
						</ul>
					</li>
					<li><a href="#">Real</a></li>
				</ul>
			</li>
			<li><a href="#">Vòng Tay</a>
				<ul>
					<li><a href="#">Chelsea</a></li>
					<li><a href="#">Manu</a>
						<ul>
							<li><a href="#">Martial</a></li>
							<li><a href="#">Vidic</a></li>
							<li><a href="#">Degea</a></li>
						</ul>
					</li>
					<li><a href="#">Real</a></li>
				</ul>
			</li>
			<li><a href="#">Sân Bóng</a>
				<ul>
					<li><a href="#">Arsenal</a></li>
					<li><a href="#">Liverpood</a>
						<ul>
							<li><a href="#">Sân Cõ Nhỏ</a></li>
							<li><a href="#">Sân Cõ Vừa</a></li>
							<li><a href="#">Sân Cỡ To</a></li>
						</ul>
					</li>
					<li><a href="#">Mancity</a></li>
				</ul>
			</li>
			<li><a href="#">Tượng Bóng rổ</a>
				<ul>
					<li><a href="#">test2.1</a></li>
					<li><a href="#">test2.2</a>
						<ul>
							<li><a href="#">test2.2.1</a></li>
							<li><a href="#">test2.2.2</a></li>
							<li><a href="#">test2.2.3</a></li>
						</ul>
					</li>
					<li><a href="#">test2.3</a></li>
				</ul>
			</li>
		</ul>
		@if(Auth::check())
		<div style="display: flex;">	
			{{-- <div style="float: left;"> --}}
			<div style="padding-top: 4px;flex-grow: 1;">
				<form action="{{ route('search-all') }}" method="post" style="width: 89%;display: inline-flex;padding-left: 30px;">
					 @csrf
					<input type="text" placeholder="Nhập Từ Khóa Tìm Kiếm" class="form-control" name="name_search">
					<button class="btn btn-default">Tìm Kiếm</button>
				</form>
			</div>
			<div id="notify-current" style="padding-right: 19px;">
				<i class="far fa-bell" style="font-size: 29px;padding-top: 5px;color: white;cursor: pointer;" ></i>
				<span class="badge" style="background: #cb4437">{{ $notifies->count() }}</span>
			</div>
			<div id="cart-current">
				<a class="cart_anchor giohang2" ><i class="fa fa-shopping-cart" style="font-size: 29px;padding-top: 5px;color: white;cursor: pointer;" ></i></a>
				<span class="badge" id="count">{{ $carts->count() }}</span>
				
			</div>
			<div>
				<img src="{{ strpos(@get_headers(asset('image/'.Auth::User()->image))[0],'403')===false ? asset('image/'.Auth::User()->image) : Auth::User()->image   }}" alt="" style="height: 37px;border-radius: 50%;position: relative;left: 21px;width: 37px">
				<span style="padding-left: 34px;color: white;">{{ Auth::User()->name }}</span>                          
				<span class="dropdown">
				    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="background: #78b43d; border-color: #78b43d;" id="menu-infor">
				    <span class="caret" ></span></button>
			    </span>
			</div>
		</div>
		<div>
			{{-- <div>
				
			</div> --}}		
			<div style="float: right;display: flex;">
				<div style="width: 286px;background: #fff;display: none;position: relative;right: 259px;box-shadow: rgba(9, 6, 23, 0.84) 0px 0px 20px -5px;overflow: hidden;" id="notify">
					<div style="text-align: center;margin: 5px;">
						<strong>Thông Báo</strong>
					</div>
					@foreach($notifies as $notify)
					<div class="row" style="background: #f5f5f5;margin: 0px;">
						<div class="col-md-2">
							<img src="{{ asset('image/'.$notify->image) }}" alt="" style="height: 35px;width: 35px;margin-top: 5px">
						</div>
						<div class="col-md-10">
							<strong style="color: rgba(0,0,0,.8)">{!! $notify->title !!}</strong>
							<div style="color: rgba(0,0,0,.54)">{!! $notify->describe !!}</div>
						</div>
					</div>
					@endforeach
				</div>
				<div style="height: 234px;width: 265px;background: #f5f5f5;display: none;" id="cart" class="giohang2">
		      		<div data-spy="scroll" data-target="#myScrollspy" data-offset="10" style="height:200px;overflow-y: scroll;padding:5px; border: 1px solid #ccc;background: white;" id="scroll">
		      			{{-- <p style="display: none;" id="no-sp">Không có sản phẩm nào</p> --}}
		      			@foreach($carts as $cart)
		      			<div class="sp">
							<div class="row" >
								<div class="col-md-2 img" >
									<img src="{{ asset('image/'.$cart->products->image_main) }}" alt="" style="height: 30px;margin-top: 3px;width: 30px;">
								</div>
								<div class="col-md-5 namesp" >
									<p class="namespham" style="font-size: 12px">{{ $cart->name }}</p>
								</div>
								<div class="col-md-5 sl" >
									<span class="giasp" style="font-size: 12px">{{ $cart->gia/1000 }}.000 vnđ x</span>
									<span id="soluongsp" style="font-size: 12px">{{ $cart->soluong }}</span>
									<p class="delete" style="cursor: pointer;color: red;">Xóa</p>
									{{-- <span> {{ $cart->soluong }}</span> --}}
								</div>
							{{-- 	<div class="col-md-2" >
									<p>{{ $cart->soluong }} vnđ</p>
								</div> --}}
							</div>

							<hr style="margin-top: 4px;" >
						</div>
						@endforeach
					</div>

					<div>
						<button class="btn btn-success" style="margin-left: 53px" id="xacnhan"><a href="{{ route('oder') }}" style="text-decoration: none;color: white">Xác Nhận Mua Hàng</a></button>
					</div>						
				</div>
			</div>
			@if(Auth::User()->id==1)
				<ul class="dropdown-menu" style="position: relative;left: 515px;min-width: 112px;">
				    <li><a href="{{ route('user.index') }}">Thông Tin</a></li>
				    <li><a href="{{ route('slide.index') }}">Tạo Slide</a></li>
				    <li><a href="{{ route('notify.create') }}">Tạo Thông Báo</a></li>
				    <li><a href="{{ url('/Auth/Logout') }}">Logout</a></li>
		    	</ul>
			@else		
		    <ul class="dropdown-menu" style="position: relative;left: 515px;min-width: 112px;">
			    <li><a href="{{ route('user.index') }}">Thông Tin</a></li>
			    <li><a href="{{ route('oder') }}">Giỏ Hàng</a></li>
			    <li><a href="{{ route('history') }}">Lịch Sử</a></li>
			    <li><a href="{{ url('/Auth/Logout') }}">Logout</a></li>
		    </ul>
		    @endif	  
		</div>
		@else
			<div>
				<a href="#" style="float: right;margin: 10px;color: white;margin-right: 31px;text-decoration: none;font-size: 14px;" data-toggle="modal" data-target="#myModal-3"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
				<a href="#" style="float: right;margin: 10px;color: white;margin-right: 31px;text-decoration: none;font-size: 14px;" data-toggle="modal" data-target="#myModal-2"><span class="glyphicon glyphicon-log-in"></span> Login</a>
			</div>
			<p style="display: none;" id="wrong">{{ session('wrong') }}</p>
		@endif

		<br>
		<br>
		<div class="fanpage_tuong">
			<div style="background: #39599f;width: 28px;height: 194px;cursor: pointer;" id="fanpage">
				<div class="outer">
				    <div class="inner rotate">FANPAGE</div>
				</div>
			</div>
			<div style="width: 96%;display: none;" id="page">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1868102553297675&autoLogAppEvents=1';
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-page" data-href="https://www.facebook.com/Tuongcauthubongdamini/" data-tabs="timeline" data-width="400" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Tuongcauthubongdamini/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Tuongcauthubongdamini/">Tượng cầu thủ bóng đá mini</a></blockquote></div>
				<div id="fb-root"></div>

				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1868102553297675&autoLogAppEvents=1';
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div>
					<div class="fb-like" data-href="https://www.facebook.com/Tuongcauthubongdamini/" data-width="350" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true" style="background: #f5f5f5;padding-left: 28px;width: 400px;"></div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>

	<div class="contents">
		@yield('contents')
	</div>

	<!-- Modal-login -->
	<div id="myModal-2" class="modal" role="dialog">
	    <div class="form">
      
		    <div class="tab-content">
		  
        		<div id="login">   
          			<h1 style="color: black;">Welcome Login!</h1>
          			<div>
          				<a href="{{ route('login_facebook','facebook') }}"><button style="background:#3b5999;color: white;font-size: 18px;" class="button button-block"><i class="fab fa-facebook-f" style="position: relative;right: 75px;"></i> Đăng Nhập Bằng Facebook</button></a>
          			</div>
          			<form action="{{ route('login') }}" method="post" id="demoForm">
          				@csrf
            			<div class="field-wrap">     
				            <label class="labels">
				              Email Address<span class="req">*</span>
				            </label>
            				<input type="email" name="email" id="email-dn" class="input"/>
          				</div>
          
          				<div class="field-wrap">
				            <label class="labels">
				              Password<span class="req">*</span>
				            </label>
            				<input type="password" name="password" id="password-dn" class="input"/>
          				</div>
          
          				<p class="forgot"><a href="#">Forgot Password?</a></p>
          		
          				<button class="button button-block" id="submit" />Log In</button>
          
          			</form>

        		</div>
        
      		</div><!-- tab-content -->
      
		</div> <!-- /form -->
	</div>
	{{-- end Madal-login --}}

	{{-- modal-sign-up --}}
	<div id="myModal-3" class="modal fade" role="dialog">
		<div class="form">	
		    <div class="tab-content">
		        <div id="signup">   
          			<h1>Sign Up for Free</h1>
          
          			<form action="{{ route('register') }}" method="post" id="formregister">
          				 @csrf
	          			<div class="top-row">
	            			<div class="field-wrap">
					            <label class="labels">
					                Địa Chỉ<span class="req">*</span>
					            </label>
					            <input type="text" required autocomplete="off" name="address" class="input" id="address-dk" />
					        </div>
	        
				            <div class="field-wrap">
				                <label class="labels">
				                	Số Điện Thoại<span class="req">*</span>
				                </label>
				                <input type="text"required autocomplete="off" name="phone" class="input" id="phone-dk" />
				            </div>
	          			</div>

							<div class="field-wrap">
					            <label class="labels">
					                Name<span class="req">*</span>
					            </label>
					            <input type="text"required autocomplete="off" name="name" id="name-dk" class="input"/>
					            <div id="exist-name" style="color: red;"></div>
					        </div>

					        <div class="field-wrap">
					            <label class="labels">
					                Email Address<span class="req">*</span>
					            </label>
					            <input type="email"required autocomplete="off" name="email" id="email-dk" class="input"/>
					            <div id="exist-email" style="color: red;"></div>
					        </div>
	          
					        <div class="field-wrap">
					            <label class="labels">
					                Set A Password<span class="req">*</span>
					            </label>
					            <input type="password"required autocomplete="off" name="password" class="input"/>
					        </div>
	          
	          			<button type="submit" class="button button-block" id="submit-dk" />Get Started</button>         
          			</form>

        		</div>    
      		</div><!-- tab-content -->
		</div> <!-- /form -->
	</div>
	{{-- end Madal-sign-up --}}
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
	{!! Html::script('js/parsley.min.js') !!}
	@yield('scripts')
	<script>

		$(document).ready(function(){
			$("li").hover(function(){
		        $(this).find("ul:first").slideDown(1);
		    },function(){
		    	$(this).find("ul:first").hide(1);
		    });
		    
			$('#tang1').click(function(){
				console.log('tang');
			    var soluong = $('#soluong1').val();
			    var giatien = $('#giatien').text();
			    var money = giatien/soluong;
			    giatien= +giatien;
			    soluong++;
			    giatien=money*soluong;
			    $('#soluong1').val(soluong);
			    $('#giatien').text(giatien);
		    });

		    $('#giam1').click(function(){
		    	var soluong = $('#soluong1').val();
		    	var giatien = $('#giatien').text();
		        var money = giatien/soluong;
		        soluong--;
		        $('#soluong1').val(soluong);
		        if(soluong<=1){
		       		soluong=1;
		       		$('#soluong1').val(soluong);
		        }
			    giatien=money*soluong;
			    $('#giatien').text(giatien);
		    });

		    // $('#dangnhapngay').click(function(){
		    // 	$("#myModal-2").modal('show');
		    // });

		    // $('#dangkyngay').click(function(){
		    // 	$("#myModal-2").modal('hide');
		    // });
		    $("#cart-current,#cart").hover(function(){
		        $("#cart").show();
		    },function(){
		    	$("#cart").hide();
		    });

		    $("#notify-current,#notify").hover(function(){
		        $("#notify").show();
		    },function(){
		    	$("#notify").hide();
		    });

		 	$("#fanpage").click(function(){
				// console.log('anc');
		        $("#page").show();
		    });

		    $(".fanpage_tuong").mouseleave(function(){
		     	$("#page").hide();
		    });

			$("#demoForm").validate({
					onfocusout: false,
					onkeyup: false,
					onclick: false,
					rules: {
						"email": {
							required: true,
						},
						"password": {
							required: true,
							minlength: 8
						},
						"re-password": {
							equalTo: "#password",
							minlength: 8
							
						}
					},
					messages: {
						"email": {
							required: "Bắt buộc nhập email",
						},
						"password": {
							required: "Bắt buộc nhập password",
							minlength: "Hãy nhập ít nhất 8 ký tự"
						},
						"re-password": {
							equalTo: "Hai password phải giống nhau",
							minlength: "Hãy nhập ít nhất 8 ký tự"
						}
					}
				});

			$("#formregister").validate({
					onfocusout: false,
					onkeyup: false,
					onclick: false,
					rules: {
						"address": {
							required: true,
						},
						"phone": {
							required: true,
							maxlength:15
						},
						"name": {
							required: true,
							maxlength:15
						},
						"email": {
							required: true,
						},
						"password": {
							required: true,
							minlength: 8
						},
						"re-password": {
							equalTo: "#password",
							minlength: 8
							
						}
					},
					messages: {
						"address": {
							required: "Bắt buộc nhập địa chỉ",
						},
						"phone": {
							required: "Bắt buộc nhập số điện thoại",
							maxlength: "Hãy nhập ít nhất 15 ký tự"
						},
						"name": {
							required: "Bắt buộc nhập User",
							maxlength: "Nhập nhiều nhất 15 ký tự"
						},
						"email": {
							required: "Bắt buộc nhập email",
						},
						"password": {
							required: "Bắt buộc nhập password",
							minlength: "Hãy nhập ít nhất 8 ký tự"
						},
						"re-password": {
							equalTo: "Hai password phải giống nhau",
							minlength: "Hãy nhập ít nhất 8 ký tự"
						}
					}
				});
			// var b;
			// $('#name-dk').change(function() {
			// 	var name = $(this).val();
			// 	console.log(name);
			// 	 $.ajaxSetup({
			// 		headers: {
			// 				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			// 				}
			// 		});
			// 	  $.ajax({
			// 	      url: "http://localhost/E-commerce/public/checks",
			// 	      type: 'POST',
			// 	      data: {'name':name},
			// 	      success: function(result){
			// 	      	console.log(result);
			// 	              if(result > 0){
			//                      b=1;
			//                  }
			//                  else{
			//                      b=2;
			//                  }   
			// 	          }
			// 	      });		   	
			//  });
			var c;
			$('#email-dk').change(function() {
				var email = $(this).val();
				console.log(email);
				 $.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.ajax({
				    url: "http://localhost/E-commerce/public/checkemail-dk",
				    type: 'POST',
				    data: {'email':email},
				    success: function(result){
				    console.log(result);
				        if(result > 0){
			                c=1;
			            }
			            else{
			                c=2;
			            }   
				    }
				});		   	
			});

			$('#submit-dk').click(function() {
				// console.log('b='+b+'c='+c)
				if(c == 1){
					// if(b==1&&c==2){
					// 	$('#exist-name').html('<p>Đã Tồn Tại Tên Đăng Nhập</p>')
					// 	$('#exist-email').html('')
					// }
					// else if(c==1&&b==2){
					// 	$('#exist-email').html('<p>Đã Tồn Tại Email</p>')
					// 	$('#exist-name').html('')
					// }
					// else{
						// $('#exist-name').html('<p>Đã Tồn Tại Tên Đăng Nhập</p>')
					$('#exist-email').html('<p>Đã Tồn Tại Email</p>')
					// }

                     return false;                   
                }
                else{
                 	// $('#exist-name').html('')
                 	$('#exist-email').html('')
                    return true;
                }
			});

			$('#menu-infor').click(function(){
				$('.dropdown-menu').show()
			});

			$('.contents').click(function(){
				$('.dropdown-menu').hide()
			})
		});
	</script>
</body>
</html>