<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $Request)
    {
        // $validator = Validator::make($Request->all(), [
        //     'name' => ['required', 'string', 'max:255','unique:users'],
        //     'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //     'password' => ['required'],
        // ]);
        // $validator = $this->validate($Request, array(
        //         'name' => 'required|string|max:255|unique:users',
        //         'email' => 'required|string|unique:users',
        //         'password' => 'required'
        // ));
        // if($validator->fails()){
        //      return redirect()->route('home')->withErrors($validator)->with('check','dangky');
        // };
        $user = new User;
        $user->phone = $Request['phone'];
        $user->address = $Request['address'];
        $user->name = $Request['name'];
        $user->email = $Request['email'];
        $user->password = bcrypt($Request['password']);
        $user->image = '1543484185.jpg';
        $user->save();
        // User::create([
        //     'phone' =>  $Request['phone'],
        //     'address' => $Request['address'],
        //     'name' => $Request['name'],
        //     'email' => $Request['email'],
        //     'password' => bcrypt($Request['password']),
        // ]);
        // dd($user);
        return redirect()->route('home');
    }

     public function check(Request $request)
    {
        $name = User::all()->where('name',$request->name)->first();
        
        if(!is_null($name))
        {
            return 1;
        }
        else{
            return 0;
        } 
    }

    public function checkemail(Request $request){
        $email = User::all()->where('email',$request->email)->first();
        if(!is_null($email))
        {
            return 1;
        }
        else{
            return 0;
        } 
    }
}
