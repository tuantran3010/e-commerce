<html>
<head>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<div>
	<h3>Cảm ơn {{ $user['name'] }} đă mua hàng</h3>
	<p>Hãy trải nghiệm dịch vụ của chúng tôi </p>
	<div>
		<table class="table">
                <thead>
                <tr>
                    <th>tên sản phẩm</th>
                    <th>giá tiền</th>
                </tr>
                </thead>
                <tbody>
                @foreach($user['oder'] as $oder)
                    <tr>
                        <th>{{ $oder->name }}</th>
                        <th>{{ $oder->gia }} vnđ</th>
                    </tr>
                @endforeach
                </tbody>
        </table>
        <div>
        	<p style="float: right;">Tổng : 700.000 vnđ</p>
        </div>
	</div>
</div>
</body>
</html>