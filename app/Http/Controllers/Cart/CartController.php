<?php

namespace App\Http\Controllers\Cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Cart;
use App\Tag;
use App\products;

class CartController extends Controller
{
    public function delete(Request $request){
    	$carts = Auth::User()->carts;
        $check = $carts->where('name', $request->name)->first();
        $check->delete();
    }
    public function additem(Request $request)
    {
        $cart = new Cart;
        $user = Auth::User();
        $products = products::all();
        $check = Auth::User()->carts;
        $checks = $check->where('name', $request->name)->first();
        if(!isset($checks->name)){
            $product = $products->where('name', $request->name)->first();
                $cart->name = $request->name;
                $cart->soluong = 1;
                $cart->gia = $product->price;
                $cart->tong = '100000';
                $cart->products()->associate($product);
                $cart->user()->associate($user);
                $cart->save();
        }
        else{
            $checks->soluong = $checks->soluong+1;
            $checks->save();
        }
    }
    public function additems(Request $request)
    {
        $cart = new Cart;
        $user = Auth::User();
        $products = products::all();
        $check = Auth::User()->carts;;
        $checks = $check->where('name', $request->name)->first();
        if(!isset($checks->name)){
            $product = $products->where('name', $request->name)->first();
                $cart->name = $request->name;
                $cart->soluong = (int)$request->soluong;
                $cart->gia = $product->price;
                $cart->tong = '100000';
                $cart->products()->associate($product);
                $cart->user()->associate($user);
                $cart->save();
        }
        else{
            $checks->soluong = $checks->soluong+(int)$request->soluong;
            $checks->save();
        }
    }
    public function update(Request $request)
    {
        $checks = Auth::User()->carts;
        $check = $checks->where('name', $request->name)->first();
        $check->soluong = (int)$request->soluong;
        $check->save();
    }
}
