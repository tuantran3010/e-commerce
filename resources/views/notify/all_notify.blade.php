@extends('main')
@section('contents')
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-12">
            <h1>Notifies</h1>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Describe</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($notifies as $notify)
                    <tr>
                        <th><a href="{{ route('notify.show', $notify->id) }}">{{ $notify->title }}</a></th>
                        <th>
                            {!! $notify->describe !!}
                        </th>
                        <th>
                            <a href="{{ route('notify.edit', $notify->id) }}" class="btn btn-primary btn-block" style="float: left;"> Edit </a>
                             {!! Form::open(['route'=>['notify.destroy',$notify->id],'method'=>'DELETE']) !!}
                                {{ Form::submit('Delete',['class'=>'btn btn-danger btn-block']) }}
                            {!! Form::close() !!}
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
	</div>
</div>
@stop