<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    public function categories(){
		return $this->belongsTo('App\Categories');
	}
	public function comments(){
		return $this->hasMany('App\Comments');
	}
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
    public function carts(){
		return $this->hasMany('App\Cart');
	} 
	public function oders(){
		return $this->hasMany('App\Oder');
	}   
	// public function presentPrice()
 //    {
 //        return asDollars($this->price);
 //    }
 //    public function asDollars($value) {
	//     return '$' . number_format($value, 2);
	// }
}
