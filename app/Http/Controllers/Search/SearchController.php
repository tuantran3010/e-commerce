<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Cart;
use App\products;
use App\Categories;
use GMaps;
use DB;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function tuongbongda(){
    	$user = Auth::User();
    	$categories = Categories::all()->where('name','Tượng Bóng Đá');		
		$products = products::all()->where('categories_id',$categories[0]->id);
		$carts = Auth::User()->carts;

		foreach($products as $product){
			if(!is_null($product->Comments))
				{
					$product->votes = (float)$product->Comments->avg('star');
				}
			else
				{
					$product->votes=0;
				}
			$product->save();

			$int = (int) $product->votes;
			$float = $product->votes - $int;
			$danhgia = array();
			for($i=0;$i<$int;$i++){
				$danhgia[$i]=100;
			}
			$danhgia[$int]=(int)($float*100);		
			if($int<4){ 
				for($i=$int+1;$i<5;$i++){
					$danhgia[$i]=0;
				}
			}
			$danh_gia_star = collect($danhgia);
			$product->stars = $danh_gia_star;
			$product->save();
		}

    	return view('search/search')->withCarts($carts)->withUser($user)->withProducts($products)->withTitles('Tượng Bóng Đá');
    }

    public function vongtaybongda(){
    	$user = Auth::User();
    	$categories = Categories::all()->where('name','Vòng Tay Bóng Đá')->first();		
		$products = products::all()->where('categories_id',$categories->id);
		$carts = Auth::User()->carts;

		foreach($products as $product){
			if(!is_null($product->Comments))
				{
					$product->votes = (float)$product->Comments->avg('star');
				}
			else
				{
					$product->votes=0;
				}
			$product->save();

			$int = (int) $product->votes;
			$float = $product->votes - $int;
			$danhgia = array();
			for($i=0;$i<$int;$i++){
				$danhgia[$i]=100;
			}
			$danhgia[$int]=(int)($float*100);		
			if($int<4){ 
				for($i=$int+1;$i<5;$i++){
					$danhgia[$i]=0;
				}
			}
			$danh_gia_star = collect($danhgia);
			$product->stars = $danh_gia_star;
			$product->save();
		}

    	return view('search/search')->withCarts($carts)->withUser($user)->withProducts($products)->withTitles('Vòng Tay Bóng Đá');
    }

    public function search_all(Request $request)
    {
    	$search = $request->name_search;
    	$products = DB::table('products')->where('name','like','%'.$search.'%')->get();
    	$carts = Auth::User()->carts;
    	$products_all = products::all();
    	$test = array();
    	$i=0;
    	foreach($products as $product){
    		$test[$i] = $products_all->where('name', $product->name)->first();
    		$i++;
    	}

    	$results = collect($test);
    	foreach($results as $product){
			if(!is_null($product->Comments))
				{
					$product->votes = (float)$product->Comments->avg('star');
				}
			else
				{
					$product->votes=0;
				}
			$product->save();

			$int = (int) $product->votes;
			$float = $product->votes - $int;
			$danhgia = array();
			for($i=0;$i<$int;$i++){
				$danhgia[$i]=100;
			}
			$danhgia[$int]=(int)($float*100);		
			if($int<4){ 
				for($i=$int+1;$i<5;$i++){
					$danhgia[$i]=0;
				}
			}
			$danh_gia_star = collect($danhgia);
			$product->stars = $danh_gia_star;
			 $product->save();
			// dd($product);
		}

    	return view('search/search')->withCarts($carts)->withProducts($results)->withTitles($search);
    }
}
