<?php
	$API_key    = 'AIzaSyANLk62gNqcX0Mcgz1cfPBLeRjTIrtmDKQ ';
	$channelID  = 'UCaguIWhr4SMV7X5qrfWG1VQ';
	$maxResults = 10;

	$videoList = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.'&maxResults='.$maxResults.'&key='.$API_key.''));	
	
	?>

@extends('main')
@section('contents')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php 

					foreach($videoList->items as $item){
				    //Embed video
				    if(isset($item->id->videoId)){
				        echo '<div class="youtube-video">
				        		<h2>'. $item->snippet->title .'</h2>
				                <iframe width="800" height="500" src="https://www.youtube.com/embed/'.$item->id->videoId.'" frameborder="0" allowfullscreen></iframe>
				            </div>';
				    }

				}
			?>
			</div>		
		</div>			
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@stop