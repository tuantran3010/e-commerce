<?php

namespace App\Http\Controllers\Social;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\SocialAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAuthController extends Controller
{
    public function redirect($social)
    {
        // dd($social);
        return Socialite::driver($social)->redirect();
    }

    public function callback($social)
    {
    	// dd('abc');
        $user = $this->createOrGetUser(Socialite::driver($social)->user(), $social);
        // dd($user);
        auth()->login($user);

        return redirect()->to('/home');
    }

    public function createOrGetUser(ProviderUser $providerUser, $social)
    {
        $account = SocialAccount::whereProvider($social)
            ->whereProviderUserId($providerUser->getId())
            ->first();
         
        if ($account) {
            return $account->user;
        } else {
            $email = $providerUser->getEmail() ?? $providerUser->getNickname();
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $social
            ]);

            // dd($account);
            $user = User::whereEmail($email)->first();

            if (!$user) {
            	// dd($providerUser->getAvatar());
                $user = User::create([
                    'email' => $email,
                    'name' => $providerUser->getName(),
                    'password' => $providerUser->getName(),
                    'image' => $providerUser->getAvatar(),
                ]);
            }
            // dd($user);
            $account->user()->associate($user);
            $account->save();
            // dd($account);
            return $user;
        }
    }
}
