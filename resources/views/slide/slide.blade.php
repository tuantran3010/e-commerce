@extends('main')
@section('contents')
	<div class="container">
		<div>
			<h1 style="text-align: center;">Change to slide</h1>
		</div>
		<div class="row" style="margin-bottom: 50px">
			{{-- <div class="col-md-8 col-md-offset-2">	 --}}
				<div id="myCarousel" class="col-md-8 col-md-offset-2 carousel slide" data-ride="carousel">
				    <!-- Indicators -->
					    <ol class="carousel-indicators" style="z-index: 1;">
					        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					        <li data-target="#myCarousel" data-slide-to="1"></li>
					        <li data-target="#myCarousel" data-slide-to="2"></li>
					    </ol>

				    <!-- Wrapper for slides -->
					    <div class="carousel-inner">
					        <div class="item active">
					            <img src="{{ asset('image/anhnenronaldo1.jpg') }}" alt="Los Angeles" style="width:100%;max-height: 375px;" id="slide-1">
					        </div>

					        <div class="item">
					        	<img src="{{ asset('image/anhnenmessi.jpg') }}" alt="Chicago" style="width:100%;max-height: 375px;" id="slide-2">
					        </div>
					    
					        <div class="item">
					        	<img src="{{ asset('image/anhnenronaldo1.jpg') }}" alt="New york" style="width:100%;max-height: 375px;" id="slide-3">
					        </div>
					    </div>

				    <!-- Left and right controls -->
					    <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="left:14px;">
					        <span class="glyphicon glyphicon-chevron-left"></span>
					        <span class="sr-only">Previous</span>
					    </a>
					    <a class="right carousel-control" href="#myCarousel" data-slide="next" style="right:14px;">
					        <span class="glyphicon glyphicon-chevron-right"></span>
					        <span class="sr-only">Next</span>
					    </a>
				</div>
			{{-- </div> --}}
			<div class="col-md-2">
				{!! Form::open(array('route'=>'slide.store','files' => true,'data-parsley-validate'=>'')) !!}	
						{{ Form::label('slide1','Thay Đổi Avatar') }}
						{{ Form::file('slide1',array('id'=>'slide1')) }}
						{{ Form::label('slide2','Thay Đổi Avatar') }}
						{{ Form::file('slide2',array('id'=>'slide2')) }}
						{{ Form::label('slide3','Thay Đổi Avatar') }}
						{{ Form::file('slide3',array('id'=>'slide3')) }}
						{{ Form::submit('Save',array('class'=>'btn btn-success btn-lg btn-block','style'=>'margin:45px')) }}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div>
	@include('footer')
	</div>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$("#slide1").change(function(){
			console.log(this);
			let url = URL.createObjectURL(this.files[0]);
			console.log(url);
			$('#slide-1').attr('src', url);
			$('#slide-1').width(880);
			$('#slide-1').height(400);
		})
		$("#slide2").change(function(){
			// console.log(this);
			let url = URL.createObjectURL(this.files[0]);
			// console.log(url);
			$('#slide-2').attr('src', url);
			$('#slide-2').width(880);
			$('#slide-2').height(400);
		})
		$("#slide3").change(function(){
			// console.log(this);
			let url = URL.createObjectURL(this.files[0]);
			// console.log(url);
			$('#slide-3').attr('src', url);
			$('#slide-3').width(880);
			$('#slide-3').height(400);
		})
	</script>
@endsection