@extends('main')
@section('contents')
	<div class="container">
		<br>
		<div class="row"> 
			<div>
				<div class="col-md-2">
					<div class="menu-col" style="padding-left: 5px">
						<div class="bar-main">
							<div class="bar-menu">
									<span>MENU</span>
									<i class="fas fa-bars bar" id="dl-bar"></i>
							</div>
						</div>
						<div class="ul-ap">
						<ul>
							
							<li>
								<a href="{{ route('tuongbongda') }}">Tượng Bóng Đá</a>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span><a href="{{ route('vongtaybongda') }}">Vòng Tay Bóng Đá</a></span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span>Sân Bóng Đá</span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span>Áo Bóng Đa</span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span>Thẻ Đeo Bóng Đá</span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span>Đèn Ngủ CLB</span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span>Tượng Bóng Rổ</span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
							<li>
								<span>Đồ Chơi Thể Thao</span>
								<i class="fas fa-chevron-right bar"></i>
							</li>
						</ul>
						</div>
					</div>
				</div>
			</div>
		    <div id="myCarousel" class="col-md-8 carousel slide" data-ride="carousel">
		    <!-- Indicators -->
			    <ol class="carousel-indicators" style="z-index: 1;">
			        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			        <li data-target="#myCarousel" data-slide-to="1"></li>
			        <li data-target="#myCarousel" data-slide-to="2"></li>
			    </ol>

		    <!-- Wrapper for slides -->
			    <div class="carousel-inner">
			        <div class="item active">
			            <img src="{{ asset('image/'.$slides[0]) }}" alt="Los Angeles" style="width:100%;max-height: 375px;">
			        </div>

			        <div class="item">
			        	<img src="{{ asset('image/'.$slides[1]) }}" alt="Chicago" style="width:100%;max-height: 375px;">
			        </div>
			    
			        <div class="item">
			        	<img src="{{ asset('image/'.$slides[2]) }}" alt="New york" style="width:100%;max-height: 375px;">
			        </div>
			    </div>

		    <!-- Left and right controls -->
			    <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="left:14px;">
			        <span class="glyphicon glyphicon-chevron-left"></span>
			        <span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control" href="#myCarousel" data-slide="next" style="right:14px;">
			        <span class="glyphicon glyphicon-chevron-right"></span>
			        <span class="sr-only">Next</span>
			    </a>
			</div>
			<div class="col-md-2">
				<div class="news-top">
					<ul>
						<li>
							<i class="fas fa-home iconnews"></i>
							<span class="news">Trang Chủ</span>
							
						</li>
						<li>
							<a href="{{ route('intro') }}"  style="color: white;text-decoration: none">
								<i class="fas fa-globe-americas iconnews"></i>
								<span class="news">Giới Thiệu</span>
							</a>			
						</li>
						<li>							
							<i class="fas fa-book-open iconnews"></i>
							<span class="news">Tin Tức</span>							
						</li>
						<li>
							<a href="{{ route('video') }}"  style="color: white;text-decoration: none">
								<i class="fab fa-youtube iconnews"></i>
								<span class="news">Video</span>
							</a>							
						</li>
						<li>
							<i class="fas fa-futbol iconnews"></i>
							<span class="news">Sản Phẩm</span>
							
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div> 
	<br>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sort-bar">
					<span class="sort">Sắp Xếp Theo</span>
					<div>
						<div class="sort fix">
							<button class="btn btn-default " style="background: #00AFF0;color: white;" id="tuong">Tượng Bóng Đá</button>
						</div>
						<div class="sort fix">
							<button class="btn btn-default " id="vongtay">Vòng Tay Bóng Đá</button>
						</div>						
						
						<div class="sort fix">
							<select class="form-control form-control-lg">
							  	<option>Giá</option>
							  	<option> < 100000 vnđ</option>
							  	<option> > 100000 vnđ</option>
							</select>
						</div>
						@if(Auth::check())
							 @if(Auth::User()->can('add_product'))
						<div class="sort fix">
							<a href="{{ url('admin/products') }}"><button class="btn btn-default ">Thêm Sản Phẩm</button></a>
						</div>
							@endif
						@endif
					    <form class="navbar-form navbar-left" action="/action_page.php" >
						     <div class="form-group">
						        <input type="text" class="form-control" placeholder="Search" id="myInput">
						     </div>
						     <button type="submit" class="btn btn-default">Tìm Kiếm</button>
						</form>							
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="container">
		<div class="row" id="myTable">
			@foreach($products as $product)
				<div class="col-md-3" id="khung">
					<div class="items">
						<div class="image-item">
							<img src="{{ asset('image/'.$product->image_main) }}" alt="" class="size-image avataritem" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">
							@if(Auth::check())
							 	@if(Auth::User()->can('update',$product))
							<a href="{{ route('products.edit', $product->id) }}" id="edit"><i class="far fa-edit edit"></i></a>
								@endif
							@endif
						</div>
						<div class="title-item">
							<p style="cursor: pointer;"><a href="{{ route('show_product', $product->id) }}">{{ $product->name }}</a></p>
						</div>
						<div class="price-item">
							<p style="float: left;">{{ $product->price/1000 }}.000 vnđ</p>
								@if( $product->stars[0] ==0)
									<div style="padding-left: 71px;">
										<span style="border-left: solid 1px #000;padding-left: 2px;">Chưa Có Đánh Giá</span>
									</div>
								@else
							    <div style="padding-left: 88px">
						  			<div style="display: flex;" id="star">
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[0] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[1] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>				
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[2] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[3] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
										<div class="star-eluvation">
											<span class="tyle" style="width: {{ $product->stars[4] }}%">
												<i class="fas fa-star color"></i>
											</span>
										</div>
									</div>
							  		<div style="display: flex;position: relative;bottom: 16px;color:yellow;">
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
										<i class="far fa-star color"></i>
									</div>
								</div>
							@endif
						</div>
						@if(Auth::check())
						<div class="giohang">
							<button class="btn btn-primary " style="margin-top: -3px;margin-left: 32px;" id="themhang">Thêm Vào Giỏ Hàng</button>
						</div>
						@else
						<div class="login-to-buy">
							<button class="btn btn-primary" style="margin-top: -3px;margin-left: 32px;">Thêm Vào Giỏ Hàng</button>
						</div>
						@endif
					</div>
				</div>
			@endforeach	
		</div>
	
		<div>	
			<ul class="pagination" style="margin-left: 488px;">
			    {{ $products->links() }}
			</ul>
		</div>
	</div>
	
	<div>
		@include('footer')
	</div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	{{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}
	<script>
		$(document).ready(function(){

		    $('#tang').click(function(){
		    	var soluong = $('#soluong').val();
		        soluong++;
		        $('#soluong').val(soluong);
		    });

		    $('#giam').click(function(){
		    	var soluong = $('#soluong').val();
		        soluong--;
		        $('#soluong').val(soluong);
		        if(soluong<=1){
		       		soluong=1;
		       		$('#soluong').val(soluong);
		        }
		    });

		    $('#button-image-1').hover(function(){		        
		    	$("#image-main-1").css("display", "block");
		    	$("#image-main-2").css("display", "none");
		    	$("#image-main-3").css("display", "none");
		    });

		    $('#button-image-1').click(function(){		        
		    	$("#image-main-4").css("display", "block");
		    	$("#image-main-5").css("display", "none");
		    	$("#image-main-6").css("display", "none");
		    });

		    $('#button-image-2').click(function(){		        
		    	$("#image-main-5").css("display", "block");
		    	$("#image-main-4").css("display", "none");
		    	$("#image-main-6").css("display", "none");
		    });

		    $('#button-image-3').click(function(){		        
		    	$("#image-main-6").css("display", "block");
		    	$("#image-main-4").css("display", "none");
		    	$("#image-main-5").css("display", "none");
		    });

		    $('#button-image-2').hover(function(){		        
		    	$("#image-main-1").css("display", "none");
		    	$("#image-main-2").css("display", "block");
		    	$("#image-main-3").css("display", "none");
		    });

		    $('#button-image-3').hover(function(){		        
		    	$("#image-main-1").css("display", "none");
		    	$("#image-main-2").css("display", "none");
		    	$("#image-main-3").css("display", "block");
		    });

		    $("#myInput").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#myTable .items .title-item").filter(function() {
			     	$($(this).parent().parent()).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			    });
  			});

		    var cart = [
		     	{
		     		name:'',
		     		soluong:0,
		     		image:'',
		     		gia:''
		     	}
		    ];
		    var n=0;
		    var m=0;
		    var check=0;
		    var check2=0;		      
		    var scroll = $('#scroll').children('.sp');

		    for(var i = 0;i<($(scroll).length);i++){
		    	var ob = new Object;
		    	ob.name = $($(scroll[i]).children()[0]).children('.namesp').children('p').text();
		    	ob.image = $($(scroll[i]).children()[0]).children('.img').children().attr('src');
		    	ob.soluong = +$($($(scroll[i]).children()[0]).children('.sl').children()[1]).text();
		    	ob.gia = $($($(scroll[i]).children()[0]).children('.sl').children()[0]).text();
		    	cart[n]=ob;
		    	n=n+1;
		    };
			console.log(cart);

		    $(document).on('click', '.giohang', function(){
		    	console.log('abc');
		    	var parent = $(this).parent();
		    	var image = $(parent).children('.image-item').find('.avataritem');
		        var flyerClone = $(image).clone();
		        var goto_X = $(image).offset().left;
			    var goto_Y = $(image).offset().top;

		        $(flyerClone).css({
			        "position": "absolute",
			        "top": goto_Y-30,
			        "left": goto_X+200,
			        "border-radius":"50%",
			        "height":"50px",
			        "width":"50px",
			        "opacity": "1",
			        "z-index": "1000"
			    }).appendTo('body');
		        var gotoX = $('.giohang2').offset().left;
			    var gotoY = $('.giohang2').offset().top;
			    $(flyerClone).animate({
			         opacity: '0.4',
			         left: gotoX,
			         top: gotoY
			        // width: '$(flyingTo).width()'+'px',
			        // height: '$(flyingTo).height()'+'px'
			    });
			    var count = $("#count").text();
			    
			    setTimeout(function(){ 
			    	$(flyerClone).remove();
			    }, 400);
			
			    var item = new Object();
			    var namesp = $(parent).children('.title-item').children().text();

			    cart.forEach(function(sanpham){
			    	if(sanpham.name==$(parent).children('.title-item').children().text())
			    	{
			    		console.log('acbc');
			    		sanpham.soluong = sanpham.soluong+1;
			    		check=1;
			    		for(var i = 0;i<($('#scroll').children('.sp').length);i++){
			    			// console.log($($($($('#scroll').children()[i]).children()[0]).children()[1]).children('p').text());
			    			// console.log($($('#scroll').find('.namesp').text()));
			    			// console.log('abc');
							if($($($($('#scroll').children()[i]).children()[0]).children()[1]).children('p').text()==namesp)
							{
								// console.log('scroll');
								// console.log($($($($($('#scroll').children()[i]).children()[0]).children()[2]).children()[1]).text());
								var sl = +$($($($($('#scroll').children()[i]).children()[0]).children()[2]).children()[1]).text()+1
								$($($($($('#scroll').children()[i]).children()[0]).children()[2]).children()[1]).html(sl);
							} 					    	
						};			    	
			    	};			    	
			    });
			    
		    	if(check==0)
			    	{
			    		count++;
			   			$("#count").text(count);
			    		console.log('tesst');
			    		item.image = image;
			    		var src = item.image.attr('src');
					    item.name = $(parent).children('.title-item').children().text();
					    item.soluong = 1;
					    item.gia = $(parent).children('.price-item').find('p').text();
					    cart[n] = item;
					    console.log(item.image.attr('src'));
					    $("#scroll").append('<div class="sp"><div class="row">'+'<div class="col-md-2">'+'<img src='+item.image.attr('src')+' alt="" style ="height:30px;width:30px;">'+'</div>'+'<div class="col-md-5 namesp"><p style="font-size:12px">'+item.name+'</p></div><div class="col-md-5"><span style="font-size:12px">'+item.gia+' x '+'</span><span style="font-size:12px">'+item.soluong+'</span><p class="delete" style="cursor: pointer;color:red;">Xóa</p></div>'+'</div>'+'<hr style="margin-top: 4px;"></div>');
					    
					    cart[n].name = $(parent).children('.title-item').children().text();
					    cart[n].image = image;
					    cart[n].soluong = 1;
					    cart[n].gia = $(parent).children('.price-item').find('p').text();
					    n=n+1;
			    	}
			    				    
			    $.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.post("http://localhost/E-commerce/public/additem", {'name': namesp},function(data) {});
				check=0;
		    });
			
			$("#scroll").on("click", "p.delete", function(){
				var count = $("#count").text();
				count--;
			   	$("#count").text(count);
				var xoa = $(this).parent().parent().parent();
				$(xoa).remove();				
				var name = $(this).parent().parent().children('.namesp').children().text();
				
				for(var i = 0;i<($(scroll).length);i++){
					if(($($(scroll[i]).children()[0]).children('.namesp').children('p').text())==name)
						{
							$($(scroll[i]).children()[0]).children('.namesp').children('p').html('');
						};
				}
				cart.forEach(function(sanpham){
			    	if(sanpham.name==name)
			    	{
			    		sanpham.name='';
			    	}
			    });
				// console.log($($(scroll[1]).children()[0]).children('.namesp').children('p').text());
				 $.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.post("http://localhost/E-commerce/public/deletecart", {'name': name},function(data) {    
				});
			});

			var abc = $('#giam2').children().children();
			$(abc).click(function(){
				console.log('tang');
			    var soluong = $('#soluong1').val();
			    var giatien = $('#giatien').text();
			    var money = giatien/soluong;
			    giatien= +giatien;
			    soluong++;
			    giatien=money*soluong;
			    $('#soluong1').val(soluong);
			    $('#giatien').text(giatien);
		    });

		    $($("#star").children()).click(function(){
		    	var mang = $("#star").children();
		    	var a = $(this).children().text();
		    	var c =Number((a));
		  		var mang2= [mang[0],mang[1],mang[2],mang[3],mang[4]]
			 	mang2.forEach(function(item){
			 		console.log(item);
                	var m = $(item).children().text();
                	if(Number(m)>c){
                		$(item).removeClass("fas fa-star color");
                		$(item).addClass("far fa-star color");
                	}
                	else{
                		$(item).removeClass("far fa-star color");
                		$(item).addClass("fas fa-star color");                		
                		$(item).css("color","yellow");
                	}
				});
		    });

		    $('#vongtay').click(function(){
		    	$(this).css({
		    		"background":"#00AFF0",
		    		"color":"white"
		    	})
		    	$('#tuong').css({
		    		"background":"white",
		    		"color":"black"
		    	})
		    	
				$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
				});
				$.ajax({
		            url: "http://localhost/E-commerce/public/test1",
		            method: 'get',
		            dataType:'html',
		
		            success: function(response) {
		               // console.log(response);
						$('#myTable').html(response);
		            }
	        	});
			});

			$('#tuong').click(function(){
		    	$(this).css({
		    		"background":"#00AFF0",
		    		"color":"white"
		    	})
		    	$('#vongtay').css({
		    		"background":"white",
		    		"color":"black"
		    	})
		    	
				$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
				});
				$.ajax({
		            url: "http://localhost/E-commerce/public/tuong",
		            method: 'get',
		            dataType:'html',
		
		            success: function(response) {
		               console.log(response);
						$('#myTable').html(response);
		            }
	        	});
			});

			$("#dl-bar").click(function(){
		        $(".ul-ap").animate({
		             height: 'toggle'
		        });
		    });

		    $($(".news-top").find('li')).mouseleave(function(){
		     	 $(this).animate({
		            right:'0px'
				},'milliseconds');
		    });

		    $(".login-to-buy").click(function(){
		    	$("#myModal-2").modal('show');
		    })

		    $('#xacnhan').click(function(){
		    	if($('.sp').length==0)
		    	{  		
		    		return false;
		    	}
		    	else{		
		    		return true;
		    	}
		    })    
		});
	</script>
@stop