@extends('main')
@section('contents')
	<div class="container">
		<div class="text">
			<p>Shop-A</p>
		</div>
	</div>
	<div class="container " style="background: #f5f5f5;">
		<div class="row view frames">
			<div class="col-md-12 oder">
				<div class="component information">
					<div class="textcenter">
						<span class="icon">1</span>
						<span>Thông Tin Khách Hàng</span>
					</div>
					<br>
					<div>
					    {!! Form::model($user,array('route'=>'confirm-oder','files' => true,'data-parsley-validate'=>'')) !!}
					        {{ Form::label('name','Name') }}
					        {{ Form::text('name',null,array('class'=>'form-control','required'=>'')) }}
								@if ($errors->has('name'))									
						            <div class="alert alert-danger">
						            	<p >{{ $errors->first('name') }}</p>
						            </div>
					        	@endif
					        {{ Form::label('email','Email') }}
					        {{ Form::text('email',null,array('class'=>'form-control','required'=>'','type'=>'number')) }}
								@if ($errors->has('email'))									
						            <div class="alert alert-danger">
						            	<p >{{ $errors->first('email') }}</p>
						            </div>
					        	@endif
					        {{ Form::label('address','Địa Chỉ') }}
					        {{ Form::text('address',null,array('class'=>'form-control','required'=>'')) }}
								@if ($errors->has('address'))									
						            <div class="alert alert-danger">
						            	<p >{{ $errors->first('address') }}</p>
						            </div>
					        	@endif
					        {{ Form::label('phone','Số Điện Thoại') }}
					        {{ Form::text('phone',null,array('class'=>'form-control','required'=>'')) }}
					    		@if ($errors->has('phone'))									
						            <div class="alert alert-danger">
						            	<p >{{ $errors->first('phone') }}</p>
						            </div>
					        	@endif		
					</div>
				</div>
				<div class="component paypaid information">
					<div class="textcenter">
						<span class="icon">2</span>
						<span>Phương Thức Thanh Toán</span>
					</div>
				</div>
				<div class="component information" style="padding: 20px;">
					<div class="textcenter">
						<span class="icon">3</span>
						<span>Hóa Đơn</span>
					</div>
					<div>
						<p style="border-bottom: #4B4F6B solid 1px;">Danh Sách Sản Phẩm</p>
						@foreach($carts as $cart)
						<div>
						<div class="row totalitem">
							<div class="col-md-2">
								<img src="{{ asset('image/'.$cart->products->image_main) }}" alt="" style="height: 30px;width: 30px">
							</div>
							<div class="col-md-7 inforitem">
								<p>{{ $cart->name }}</p>
								<div>
									<input type="text" value="{{ $cart->soluong }}" style="width: 20%;">
									<span>x</span><span class="priceitem"> {{ $cart->gia/1000 }}.000 vnđ</span>
								</div>
							</div>
							<div class="col-md-3">
								<span style="cursor: pointer;float: right;">
									<i class="fas fa-trash delete"></i>							
								</span>
							</div>
						</div>
						<hr>
						</div>
						@endforeach
						<div class="row">
							<div class="col-md-6">
								<p>Tổng Tiền: </p>
								<p>Phí Ship: </p>
								<p>Mã Giảm Giá:</p>
								{{-- <p>Tổng Hóa Đơn:</p> --}}
							</div>
							<div class="col-md-6" style="text-align: right;">
								<p id="totalprice"></p>
								<p>Liên Hệ: 0967887496</p>
								<p>20%</p>
								{{-- <p>700.000 đ</p> --}}
							</div>
						</div>
						<div style="float: right;">
							<button class="btn btn-danger">Mua Thêm</button>
							{{ Form::submit('Mua Hàng', ['class' => 'btn btn-primary']) }}
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	{{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}
	<script>
		$(document).ready(function(){
			var tong = 0;
			
			for(var i=0;i<$('.totalitem').length;i++){
				var giatien = $($('.totalitem')[i]).children().children().children('span.priceitem').text()
				var priceitem = parseInt(giatien)*1000
				
				var soluong = $($('.totalitem')[i]).children().children().children('input').val()
				tong = +tong + soluong*(+priceitem)
			}
			var nghin = tong/1000
			$('#totalprice').html(nghin+'.'+'000'+'vnđ');

			$('.delete').click(function(){
				$(this).parent().parent().parent().parent().remove()
				var name = $(this).parent().parent().parent().children('.inforitem').children('p').text()
				tong = 0
				for(var i=0;i<$('.totalitem').length;i++){
					var giatien = $($('.totalitem')[i]).children().children().children('span.priceitem').text()
					var priceitem = parseInt(giatien)*1000
					// console.log(priceitem);
					var soluong = $($('.totalitem')[i]).children().children().children('input').val()
					tong = +tong + soluong*(+priceitem)
				}
				// console.log(tong)
				var nghin = tong/1000
				// console.log(nghin+'.'+'000'+' vnđ')
				$('#totalprice').html(nghin+'.'+'000'+' vnđ');

				$.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.post("http://localhost/E-commerce/public/deletecart", {'name': name},function(data) {    
				});
			})

			$('body').click(function(){
				tong = 0;
				for(var i=0;i<$('.totalitem').length;i++){
					var giatien = $($('.totalitem')[i]).children().children().children('span.priceitem').text()
					var priceitem = parseInt(giatien)*1000
					
					var soluong = $($('.totalitem')[i]).children().children().children('input').val()
					tong = +tong + soluong*(+priceitem)
				}
				var nghin = tong/1000
				$('#totalprice').html(nghin+'.'+'000'+'vnđ');
			})
			$('input').change(function(){
				name = $(this).parent().parent().children('p').text();
				soluong = $(this).val();
				$.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.post("http://localhost/E-commerce/public/updatecart", {'name': name,'soluong':soluong},function(data) {    
				});
			})

		});
	</script>
@stop
