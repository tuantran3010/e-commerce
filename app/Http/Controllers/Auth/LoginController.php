<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    protected function login(Request $request)
    {
        $this->validate($request, array(
                'email' => 'required|string',
                'password' => 'required'
        ));
        $email = $request->email;
        $password = $request->password;
        if( Auth::attempt(['email' => $email, 'password' =>$password])) {
            return redirect()->route('home');
        } else {

        }
    }
    use AuthenticatesUsers;
    // public function check(Request $request)
    // {
    //     $name = User::all()->where('name',$request->name)->first();

    //     if(!is_null($name))
    //     {
    //         return 1;
    //     }
    //     else{
    //         return 0;
    //     }     // dd('abc');
    //     // return 'abc';
    // }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
