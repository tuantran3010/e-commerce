@extends('main')
@section('contents')
	<div class="container" id="infor">
		<div style="text-align: center;">
			<h3>Thông Tin Cá Nhân</h3>
		</div>
		
		<div class="row">
			<div class="col-md-3">
			    {!! Form::model($user,array('route'=>'user.store','files' => true,'data-parsley-validate'=>'')) !!}
				<img src="{{ asset('image/'.$user->image) }}" alt="" id="profile_image_tag" style="max-width: 418px; max-height: 318px;">
					{{ Form::label('avatar','Thay Đổi Avatar') }}
        			{{ Form::file('avatar',array('id'=>'newavatar')) }}
			</div>
			<div class="col-md-7 col-md-offset-2">

			        {{ Form::label('name','Name') }}
			        {{ Form::text('name',null,array('class'=>'form-control','required'=>'')) }}
						{{-- @if ($errors->has('name'))									
				            <div class="alert alert-danger">
				            	<p >{{ $errors->first('name') }}</p>
				            </div>
			        	@endif --}}
			        {{ Form::label('email','Email') }}
			        {{ Form::text('email',null,array('class'=>'form-control','required'=>'','type'=>'number')) }}
						{{-- @if ($errors->has('email'))									
				            <div class="alert alert-danger">
				            	<p >{{ $errors->first('email') }}</p>
				            </div>
			        	@endif --}}
			        {{ Form::label('address','Địa Chỉ') }}
			        {{ Form::text('address',null,array('class'=>'form-control','required'=>'')) }}
						{{-- @if ($errors->has('password'))									
				            <div class="alert alert-danger">
				            	<p >{{ $errors->first('password') }}</p>
				            </div>
			        	@endif --}}
			        {{ Form::label('phone','Số Điện Thoại') }}
			        {{ Form::text('phone',null,array('class'=>'form-control','required'=>'')) }}

			        <br>
			        {{ Form::submit('Save',array('class'=>'btn btn-success btn-lg btn-block')) }}
			    {!! Form::close() !!}				
			</div>
		</div>
	</div>
	{!! Html::script('js/parsley.min.js') !!}
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$("#newavatar").change(function(){
			console.log(this);
			let url = URL.createObjectURL(this.files[0]);
			console.log(url);
			$('#profile_image_tag').attr('src', url);
			$('#profile_image_tag').width(400);
			$('#profile_image_tag').height(400);
		})
	</script>
@stop