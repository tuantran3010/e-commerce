<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\products;
use App\Cart;
use App\User;
use App\Categories;
use App\Comments;
use GMaps;
use Illuminate\Support\Facades\Auth;
use App\Slide;
use App\Notify;
// use Illuminate\Pagination\Paginator;
// use Illuminate\Support\Collection;
// use Illuminate\Pagination\LengthAwarePaginator;

class HomeController extends Controller
{
	public function home(){
		$notifies = Notify::all();
		$slides = Slide::all()->take(-3);
		$array_slide = array();
		$i=0;
		foreach($slides as $slide)
		{
			$array_slide[$i] = $slide->image;
			$i++;
		}
		// dd($array_slide);
		$categories = Categories::all()->where('name','Tượng Bóng Đá')->first();		
		$products = products::where('categories_id',$categories->id)->paginate(8);
		// dd($products) ;
		// $products_paginate = $this->paginate($products);
		if(!is_null(Auth::User()))
			{
				$carts = Auth::User()->carts;
			}
		else{
				$carts = null;
		}
		foreach($products as $product){
			if(!is_null($product->Comments))
				{
					$product->votes = (float)$product->Comments->avg('star');
				}
			else
				{
					$product->votes=0;
				}
			$product->save();

			$int = (int) $product->votes;
			$float = $product->votes - $int;
			$danhgia = array();
			for($i=0;$i<$int;$i++){
				$danhgia[$i]=100;
			}
			$danhgia[$int]=(int)($float*100);		
			if($int<4){ 
				for($i=$int+1;$i<5;$i++){
					$danhgia[$i]=0;
				}
			}
			// dd($danhgia);
			$danh_gia_star = collect($danhgia);
			$product->stars = $danh_gia_star;
			$product->save();
			// dd($product->stars[0]);
		}

		$config = array();
	    $config['zoom'] = '16';
		$config['map_height'] = '200px';

		$market = array();
		GMaps::initialize($config);

		GMaps::add_marker($market);
	    $map = GMaps::create_map();

    	return view('home/home')->withProducts($products)->withCarts($carts)->withMap($map)->withSlides($array_slide)->withNotifies($notifies);
	}

	// public function paginate($items, $perPage = 4, $page = null, $options = [])
	// {
	//     $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	//     $items = $items instanceof Collection ? $items : Collection::make($items);
	//     return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
	// }
}
