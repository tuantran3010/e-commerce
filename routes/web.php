<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//My_home
Route::get('home','Home\HomeController@home')->name('home');
//login_register_logout
Route::post('Auth/Register','Auth\RegisterController@create')->name('register');
Route::post('Auth/Login','Auth\LoginController@login')->name('login');
Route::get('Auth/Logout', 'Auth\LoginController@logout')->name('logout');
//manage tag,catagories,products
Route::group(['prefix'=>'admin','middleware'=>['admin']],function(){
	Route::resource('tags', 'Tags\TagsController');
	Route::resource('categories', 'Categories\CategoriesController');
	Route::resource('products', 'Products\ProductsController');
	Route::resource('slide','Slide\SlideController');
	Route::resource('notify','Notify\NotifyController');
});
//manage products
Route::get('product/{id}', 'Products\ProductsController@show')->name('show_product');

Route::post('comments/{id}','Comments\CommentsController@store')->name('comments');
//information_user
Route::group(['prefix'=>'information','middleware'=>['auth']],function(){
    Route::resource('user','Information\InformationController');
    //history
    Route::get('history','History\HistoryController@index')->name('history');
    //oder
    Route::get('oder','Oder\OderController@index')->name('oder');
});
//use_ajax_edit_cart
Route::post('additem','Cart\CartController@additem');
Route::post('deletecart','Cart\CartController@delete');
Route::post('updatecart','Cart\CartController@update');

//test api_googlemaps
Route::get('test',function(){
	$config = array();
    // $config['center'] = 'New York, USA';
    $config['zoom'] = '18';
	$config['map_height'] = '500px';

	$market = array();
	// $config['scrollwheel'] = false;

	GMaps::initialize($config);

	GMaps::add_marker($market);
    $map = GMaps::create_map();
	return view('test/test')->withMap($map);
});
//get_video_youtube
Route::get('video','Test\TestController@video')->name('video');
//send_ajax_products
Route::get('test1','Test\TestController@test');
Route::get('tuong','Test\TestController@tuong');
//add_item_to_cart
Route::post('additems','Cart\CartController@additems');
//information_shop
Route::get('intro','Information\InformationController@intro')->name('intro');
//test
Route::get('test-apiface',function(){

	return view('test/test1');
});
//check_exist_email_and_user
Route::post('checks','Auth\RegisterController@check')->name('check');
Route::post('checkemail-dk','Auth\RegisterController@checkemail')->name('checkemail');
//search
Route::get('search/tuongbongda','Search\SearchController@tuongbongda')->name('tuongbongda');
Route::get('search/vongtaybongda','Search\SearchController@vongtaybongda')->name('vongtaybongda');
Route::post('search-all/name','Search\SearchController@search_all')->name('search-all');
//confirm oder
Route::post('confirm-oder','Oder\OderController@confirm')->name('confirm-oder');

//test menu dep
Route::get('testmenu',function(){
	return view('test/test_search');
});
//test login facebook
Route::get('/redirect/{social}', 'Social\SocialAuthController@redirect')->name('login_facebook');
Route::get('/callback/{social}', 'Social\SocialAuthController@callback');
Route::get('testlogin',function(){
	return view('test/test_login_facebook');
});
