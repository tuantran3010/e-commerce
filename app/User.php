<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public function comments(){
        return $this->hasMany('App\Comments');
    }

    public function carts(){
        return $this->hasMany('App\Cart');
    }
    public function oders(){
        return $this->hasMany('App\Oder');
    }
    public function socials(){
        return $this->hasMany('App\SocialAccount');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
