<?php

namespace App\Http\Controllers\Slide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use Image;
use App\Slide;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Auth::User()->carts;
        return view('slide/slide')->withCarts($carts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('slide1'))
        {
            $slide = new Slide;
            $slide1=$request->file('slide1');
            $filename_main=time().'00'.'.'.$slide1->getClientOriginalExtension();
            //dd($filename_main);
            $localtion_main= public_path('image/'.$filename_main);
            Image::make($slide1)->resize(488,491)->save($localtion_main);
            $slide->image=$filename_main;
            $slide->save();
        }
        if($request->hasFile('slide2'))
        {
            $slide = new Slide;
            $slide2=$request->file('slide2');
            $filename_sub1=time().'01'.'.'.$slide2->getClientOriginalExtension();
            //dd($filename_sub1,$filename_main);

            //dd($product->image_main);
            $localtion_sub1= public_path('image/'.$filename_sub1);
            Image::make($slide2)->resize(488,491)->save($localtion_sub1);
            $slide->image=$filename_sub1;
            $slide->save();
        }
        if($request->hasFile('slide3'))
        {
            $slide = new Slide;
            $slide3=$request->file('slide3');
            $filename_sub2=time().'02'.'.'.$slide3->getClientOriginalExtension();
            //dd($filename_sub2,$filename_sub1,$filename_main);
            $localtion_sub2= public_path('image/'.$filename_sub2);
            Image::make($slide3)->resize(488,491)->save($localtion_sub2);
            $slide->image=$filename_sub2;
            $slide->save();
        }
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
