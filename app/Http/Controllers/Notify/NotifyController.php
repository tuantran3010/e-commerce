<?php

namespace App\Http\Controllers\Notify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Notify;
use Image;

class NotifyController extends Controller
{
    public function index()
    {
        $notifies= Notify::all();
        $carts = Auth::User()->carts;
        return view('notify/all_notify')->withCarts($carts)->withNotifies($notifies);
    }

    public function create()
    {
        // dd('abc');
        $carts = Auth::User()->carts;
        return view('notify/notify')->withCarts($carts);
    }

    public function store(Request $request)
    {
    	$notify = new Notify;
    	$notify->title = $request->title;
    	$notify->describe = $request->describe;
        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $filename_main=time().'00'.'.'.$image->getClientOriginalExtension();
            $localtion_main= public_path('image/'.$filename_main);
            Image::make($image)->save($localtion_main);
            $notify->image=$filename_main;      
        }
        $notify->save();
        return redirect()->route('home');
    }

    public function edit($id)
    {
    	$carts = Auth::User()->carts;
        $notify = Notify::find($id);
        return view('notify/edit_notify')->withNotify($notify)->withCarts($carts);
    }

    public function update(Request $request,$id)
    {
    	$notify = Notify::find($id);
    	$notify->title = $request->title;
    	$notify->describe = $request->describe;
        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $filename_main=time().'00'.'.'.$image->getClientOriginalExtension();
            $localtion_main= public_path('image/'.$filename_main);
            Image::make($image)->save($localtion_main);
            $notify->image=$filename_main;      
        }
        $notify->save();
        return redirect()->route('home');
    }

    public function destroy($id)
    {
        $notify = Notify::find($id);
        $notify->delete();

        return redirect()->route('notify.index');
    }
}
