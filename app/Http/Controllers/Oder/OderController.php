<?php

namespace App\Http\Controllers\Oder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Mail\OrderShipped;
use App\Cart;
use App\User;
use App\Oder;
use Mail;
use DB;

class OderController extends Controller
{
    public function index(){
    	$carts = Auth::User()->carts;
    	$user = Auth::User();
    	return view('oders/oders')->withCarts($carts)->withUser($user);
    }
    public function confirm(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);
    	$carts = Auth::User()->carts;
    	$user = Auth::User();
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->phone = $request->phone;
    	$user->address = $request->address;

    	$user->save();

    	foreach($carts as $cart)
    	{
    		$oder = new Oder;
	    	$oder->name = $cart->name;
	    	$oder->user_id = $cart->user_id;
	    	$oder->products_id = $cart->products_id;
	    	$oder->soluong = $cart->soluong;
	    	$oder->tong = ($cart->soluong)*(int)($cart->gia);
            $oder->save();
	    	DB::table('carts')->where('id', $cart->id)->delete(); 
            
    	}
        $carts_current = Auth::User()->carts;
        $user = array(
            'email' => 'mr.kun301096@gmail.com',
            'suject' => 'test',
            'BodyMessage'=> 'test',
            'name' => 'anhhoang',
            'oder' => Auth::User()->carts
        );
        Mail::to('mr.kun301096@gmail.com')->send(new OrderShipped($user));

    	return view('success/success')->withCarts($carts_current);	
    }
}
