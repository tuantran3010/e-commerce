@extends('main')
@section('contents')
	<div class="container">
		<div style="padding-left: 276px;">
			<h3>Lịch Sử Giao Dịch</h3>
		</div>
		
		<div class="row" >
			<div class="col-md-3" style="height: 400px;">
				<div style="display: flex;border-bottom: solid 1px #f5f5f5;">
					<div>
						<img src="{{ asset('image/'.Auth::User()->image) }}" alt="" style="max-width: 50px;border-radius: 50%;">
					</div>
					<div style="padding-left: 10px;">
						<span>tuantran</span>
						<p><a href="{{ route('user.index') }}" style="color: #888;text-transform: capitalize;">Sửa Thông Tin</a></p>
					</div>	
				</div>
				<div style="padding-top: 28px;">
					<p><i class="fas fa-user"></i><a href="{{ route('user.index') }}" style="color: black;text-decoration: none;"> Thông Tin Cá Nhân</a></p>
					<p><i class="fas fa-globe-asia"></i><a href="{{ route('intro') }}" style="color: black;text-decoration: none;"> Thông Tin Shop</a></p>
					<p><i class="fab fa-youtube"></i><a href="{{ route('video') }}"" style="color: black;text-decoration: none;"> Video Sản Phẩm</a></p>
				</div>
			</div>
			<div class="col-md-9" style="border: solid 2px #f5f5f5;background: #f5f5f5;margin-bottom: 21px;">
				@foreach($oders as $oder)
					<div class="row" style="border-bottom: solid 1px darkgrey;" id="oder_history">
						<div class="col-md-2" style="width: 10%;margin: 19px;">
							<img src="{{ asset('image/'.$oder->products->image_main) }}" alt="" style="max-width: 50px;">
						</div>
						<div class="col-md-8">
							<h3><a href="{{ route('products.show', $oder->products->id) }}" style="color: black;text-decoration: none;">{{ $oder->name }}</a></h3>
							<p><span>{{ $oder->products->price }}</span><span> x </span><span>{{ $oder->soluong }}</span></p>
							<p>Tổng : {{ $oder->tong }} vnđ</p>
						</div>
						<div class="col-md-2" style="padding-top: 19px;">
							<p>{{ date('M j,Y H:i',strtotime($oder->created_at)) }}</p>
						</div>
					</div>
				@endforeach
				<p id="no_oder">Chưa Có Đơn Hàng Nào</p>
			</div>
		</div>
	</div>
	<div>
		@include('footer')
	</div>
	<script>
		if($('#oder_history').length==0)
		{
			$('#no_oder').show();
		}
		else{
			$('#no_oder').hide();
		}
	</script>
@endsection