@extends('main')
@section('contents')
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            <h1>Categories</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $categori)
                    <tr>
                        <th>{{ $categori->id }}</th>
                        <th><a href="{{ route('categories.show', $categori->id) }}" >{{ $categori->name }}</a></th>
                        <th>
                        	<a href="{{ route('categories.edit', $categori->id) }}" class="btn btn-primary btn-sm" style="float: left;"> Edit </a>
                        	 {!! Form::open(['route'=>['categories.destroy',$categori->id],'method'=>'DELETE']) !!}
                                {{ Form::submit('Delete',['class'=>'btn btn-danger btn-sm']) }}
                            {!! Form::close() !!}
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    		
        <div class="col-md-3 col-md-offset-1 form-spacing-top">
            <div class="well">
                {!! Form::open(['route' => 'categories.store', 'method' => 'POST']) !!}
                <h2>New Categories</h2>
                {{ Form::label('name', 'Name:') }}
                {{ Form::text('name', null, ['class' => 'form-control']) }}

                {{ Form::submit('Create New Categories', ['class' => 'btn btn-primary btn-block btn-h1-spacing']) }}
                {!! Form::close() !!}
            </div>
        </div>
	</div>
		
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
    	$(document).ready(function(){
    		if($('#edit').text().length>2){
    			$("#myModal-2").modal('show');
    		}
    	});
    </script>
@stop