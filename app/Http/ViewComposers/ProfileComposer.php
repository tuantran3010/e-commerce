<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\Notify;

class ProfileComposer{
 public function compose(View $view)
    {
    	$notifies = Notify::all();
        $view->with('notifies', $notifies);
    }
}