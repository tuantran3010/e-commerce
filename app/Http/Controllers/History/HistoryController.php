<?php

namespace App\Http\Controllers\History;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cart;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index()
    {
    	$cart = Auth::User()->carts;
    	$oders = Auth::User()->oders;
    	return view('history/history')->withCarts($cart)->withOders($oders);
    }
}
