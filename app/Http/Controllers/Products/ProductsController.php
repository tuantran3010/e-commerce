<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Products;
use App\Categories;
use App\Comments;
use App\Tag;
use App\User;
use App\Cart;
use Image;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Auth::User()->carts;
        $Categories = Categories::all();
        $tags = Tag::all();
        return view('products/addproduct')->withCategories($Categories)->withTags($tags)->withCarts($carts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'mieuta' => 'required',
            'xuatxu' => 'required',
            'image_main' => 'file|required|image',
        ]);
        // dd('abc');
        $product = new Products;
        $id = $request->categories[0];
        $Categories = Categories::find($id);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->mieuta = $request->mieuta;
        $product->xuatxu = $request->xuatxu;

        if($request->hasFile('image_main'))
        {
            $image_main=$request->file('image_main');
            $filename_main=time().'00'.'.'.$image_main->getClientOriginalExtension();
            //dd($filename_main);
            $localtion_main= public_path('image/'.$filename_main);
            Image::make($image_main)->resize(488,491)->save($localtion_main);
            $product->image_main=$filename_main;
        }
        if($request->hasFile('image_sub1'))
        {
            $image_sub1=$request->file('image_sub1');
            $filename_sub1=time().'01'.'.'.$image_sub1->getClientOriginalExtension();
            //dd($filename_sub1,$filename_main);

            //dd($product->image_main);
            $localtion_sub1= public_path('image/'.$filename_sub1);
            Image::make($image_sub1)->resize(488,491)->save($localtion_sub1);
            $product->image_sub1=$filename_sub1;
        }
        if($request->hasFile('image_sub2'))
        {
            $image_sub2=$request->file('image_sub2');
            $filename_sub2=time().'02'.'.'.$image_sub2->getClientOriginalExtension();
            //dd($filename_sub2,$filename_sub1,$filename_main);
            $localtion_sub2= public_path('image/'.$filename_sub2);
            Image::make($image_sub2)->resize(488,491)->save($localtion_sub2);
            $product->image_sub2=$filename_sub2;
        }

        $product->categories()->associate($Categories);
        $product->so_luong_nguoi_mua = '0';

        $product->save();

        $product->tags()->sync($request->tags);
        return redirect()->route('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = products::find($id);
        $comments = Comments::all();
        if(Auth::User())
        { 
            $carts = Auth::User()->carts;
        }
        else
        {
            $carts=null;
        }
        $admin = User::find(1);

        if(!is_null($product->Comments))
            {
                $product->votes = (float)$product->Comments->avg('star');
            }
        else
            {
                $product->votes=0;
            }
        $product->save();

        $int = (int) $product->votes;
        $float = $product->votes - $int;
        $danhgia = array();
        for($i=0;$i<$int;$i++){
            $danhgia[$i]=100;
        }
        $danhgia[$int]=(int)($float*100);       
        if($int<4){ 
            for($i=$int+1;$i<5;$i++){
                $danhgia[$i]=0;
            }
        }

        $danh_gia_star = collect($danhgia);
        $product->stars = $danh_gia_star;
        $product->save();
        
        return view('products/items')->withProduct($product)->withComments($comments)->withCarts($carts)->withAdmin($admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carts = Auth::User()->carts;
        $product = products::find($id);     
        $Categories = Categories::all();
        $tags = Tag::all();
        $tags2 = array();
        foreach($tags as $tag){
            $tags2[$tag->id] = $tag->name;
        }
        // dd($carts);
        return view('products/editproduct')->withProduct($product)->withCategories($Categories)->withTags($tags2)->withCarts($carts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'mieuta' => 'required',
            'xuatxu' => 'required',
        ]);

        $product = products::find($id);
        // $user = Auth::User();
        
        // $this->authorize("update",$user);
        $idcat = $request->categories[0];
        $Categories = Categories::find($idcat);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->mieuta = $request->mieuta;
        $product->xuatxu = $request->xuatxu;

        if($request->hasFile('image_main'))
        {
            $image_main=$request->file('image_main');
            $filename_main=time().'00'.'.'.$image_main->getClientOriginalExtension();
            //dd($filename_main);
            $localtion_main= public_path('image/'.$filename_main);
            Image::make($image_main)->resize(488,491)->save($localtion_main);
            $product->image_main=$filename_main;
        }
        if($request->hasFile('image_sub1'))
        {
            $image_sub1=$request->file('image_sub1');
            $filename_sub1=time().'01'.'.'.$image_sub1->getClientOriginalExtension();
            //dd($filename_sub1,$filename_main);

            //dd($product->image_main);
            $localtion_sub1= public_path('image/'.$filename_sub1);
            Image::make($image_sub1)->resize(488,491)->save($localtion_sub1);
            $product->image_sub1=$filename_sub1;
        }
        if($request->hasFile('image_sub2'))
        {
            $image_sub2=$request->file('image_sub2');
            $filename_sub2=time().'02'.'.'.$image_sub2->getClientOriginalExtension();
            //dd($filename_sub2,$filename_sub1,$filename_main);
            $localtion_sub2= public_path('image/'.$filename_sub2);
            Image::make($image_sub2)->resize(488,491)->save($localtion_sub2);
            $product->image_sub2=$filename_sub2;
        }
        //dd($product->image_main);
        //dd([$product->image_sub2,$product->image_main,$product->image_sub1]);
        $product->categories()->associate($Categories);
        $product->so_luong_nguoi_mua = '0';

        $product->save();

        if(isset($request->tags)){
            $product->tags()->sync($request->tags);
        }
        else{
            $product->tags()->sync(array());
        }
        return redirect()->route('home');        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = products::find($id);
        $product->tags()->detach();
        $product->delete();

        return redirect()->route('home');
    }
}
