<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    public function products(){
		return $this->belongsTo('App\products');
	}
	public function user(){
		return $this->belongsTo('App\User');
	}

	public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
