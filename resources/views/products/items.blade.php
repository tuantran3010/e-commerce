@extends('main')
@section('contents')
	<div class="background">
	<br>
		<div class="container" style="background: white;">
			<br>
			<div class="row">
				<div class="col-md-4">
					<div class="imageproduct">
						<img src="{{ asset('image/'.$product->image_main) }}" alt="" style="height: 360px;display: block;" id="image-main-1" class="sizes-image">
						<img src="{{ asset('image/'.$product->image_sub1) }}" alt="" class="sizes-image" id="image-main-2" style="display: none;height: 360px;">
						<img src="{{ asset('image/'.$product->image_sub2) }}" alt="" class="sizes-image" id="image-main-3" style="display: none;height: 360px;">
					</div>
					<div style="padding-top: 16px;">
						<button  data-toggle="modal" data-target="#myModal-1" id="button-image-1"><img src="{{ asset('image/'.$product->image_main) }}" alt="" class="image-product" id="image-main-1"></button>
						<button  data-toggle="modal" data-target="#myModal-1" id="button-image-2"><img src="{{ asset('image/'.$product->image_sub1) }}" alt="" class="image-product" id="image-main-2"></button>
						<button  data-toggle="modal" data-target="#myModal-1" id="button-image-3"><img src="{{ asset('image/'.$product->image_sub2) }}" alt="" class="image-product" id="image-main-3"></button>
					</div>
				</div>
				<div class="col-md-8">
					<div><p style="font-size: 21px" id="tensp">{{ $product->name }}</p></div>
					<div>
						@if( $product->stars[0] ==0)
							<div >
								<span >Chưa Có Đánh Giá</span>
							</div>
						@else
					    <div style="">
				  			<div style="display: flex;" id="star">
								<div class="star-eluvation">
									<span class="tyle" style="width: {{ $product->stars[0] }}%">
										<i class="fas fa-star color"></i>
									</span>
								</div>
								<div class="star-eluvation">
									<span class="tyle" style="width: {{ $product->stars[1] }}%">
										<i class="fas fa-star color"></i>
									</span>
								</div>				
								<div class="star-eluvation">
									<span class="tyle" style="width: {{ $product->stars[2] }}%">
										<i class="fas fa-star color"></i>
									</span>
								</div>
								<div class="star-eluvation">
									<span class="tyle" style="width: {{ $product->stars[3] }}%">
										<i class="fas fa-star color"></i>
									</span>
								</div>
								<div class="star-eluvation">
									<span class="tyle" style="width: {{ $product->stars[4] }}%">
										<i class="fas fa-star color"></i>
									</span>
								</div>
							</div>
					  		<div style="display: flex;position: relative;bottom: 16px;color:yellow;">
								<i class="far fa-star color"></i>
								<i class="far fa-star color"></i>
								<i class="far fa-star color"></i>
								<i class="far fa-star color"></i>
								<i class="far fa-star color"></i>
							</div>
							<div style="position: relative;bottom: 32px;left: 86px;">
								<p style="border-left: solid 1px;padding-left: 10px;">{{ $product->comments->count() }} đánh giá</p>
							</div>
						</div>
						@endif
					</div>
					@if( $product->stars[0] ==0)
						<div class="background" style="height: 70px;">
							<p style="color: red;position: relative;top: 19px;left: 50px;font-size: 22px;float: left;" id="giasp">{{ $product->price/1000 }}.000 vnđ</p>
							<span class="label label-warning" style="position: relative;top: 23px;left: 60px;">Giảm giá 15%</span>	
						</div>
					@else
						<div class="background" style="height: 70px;margin-top: -29px;">
							<p style="color: red;position: relative;top: 19px;left: 50px;font-size: 22px;float: left;" id="giasp">{{ $product->price/1000 }}.000 vnđ</p>
							<span class="label label-warning" style="position: relative;top: 23px;left: 60px;">Giảm giá 15%</span>	
						</div>
					@endif
					<br>
					<div>
						<div style="float: left;">
							<p>Số Lượng</p>
						</div>
						<div style="padding-left: 148px;" >
							<button style="width:3rem;" id="giam">-</button>
							<input type="text" value="1" style="width: 5rem;text-align: center;" id="soluong">
							<button style="width: 3rem;" id="tang">+</button>
						</div>
					</div>
					<hr>
					<div>
						<div>
							<p style="margin-right: 189px;float: left;">Vận Chuyển</p>
							<select class="form-control form-control-lg">
							  	<option>Hà Nội</option>
							  	<option>Sài Gòn</option>
							  	<option>Vinh</option>
							</select>
						</div>
					</div>
					<hr>
					<div>
						@if(Auth::check())
							<div class="giohang">
								<button class="btn btn-primary "  id="themhang">Thêm Vào Giỏ Hàng</button>
							</div>
							@else
							<div class="login-to-buy">
								<button class="btn btn-primary" >Thêm Vào Giỏ Hàng</button>
							</div>
						@endif
						{{-- <button class="btn btn-primary" id="them">Mua Ngay</button>							 --}}
					</div>	
				</div>
			</div>
		</div>
		<br>
		<div class="container" style="height: 100px;background: white;display: flex;">
			<div class="row" style="position: relative;top: 17px;">
				<div class="col-md-3">
					<img src="{{ asset('image/'.$admin->image) }}" alt="" style="height: 64px;width: 64px;border-radius: 50px;">
				</div>
				<div class="col-md-9">
					<p style="font-weight: 400;font-size: 2rem;color: rgba(0,0,0,.87);">{{ $admin->name }}</p>
					<a href="{{ route('intro') }}"><button class="btn btn-default" style="background:#78b43d;color: white;">Giới Thiệu</button></a>
					<a href="{{ route('home') }}"><button class="btn btn-default" style="background:#78b43d;color: white;">Xem Shop</button></a>
				</div>
			</div>
			<div class="row" style="position: relative;top: 17px;">
				<div class="col-md-3">
					<img src="{{ asset('image/shoppe.jpg') }}" alt="" style="height: 64px;width: 64px;border-radius: 50px;">
				</div>
				<div class="col-md-9">
					<p style="font-weight: 400;font-size: 2rem;color: rgba(0,0,0,.87);">trang9898</p>
					<a href="https://shopee.vn/shop/12210271/search"><button class="btn btn-default" style="background: orangered;color: white;">Shoppe</button></a>
					<a href="https://shopee.vn/trang9898"><button class="btn btn-default" style="background: orangered;color: white;">Xem Shop</button></a>
				</div>
			</div>
			<div class="row" style="position: relative;top: 17px;">
				<div class="col-md-3">
					<img src="{{ asset('image/sendo1.jpg') }}" alt="" style="height: 64px;width: 64px;border-radius: 50px;">
				</div>
				<div class="col-md-9">
					<p style="font-weight: 400;font-size: 2rem;color: rgba(0,0,0,.87);">Shop Tượng Bóng Đá Mini</p>
					<a href="https://www.sendo.vn/shop/tuong-bong-da-mini/"><button class="btn btn-default" style="background: rgb(205, 33, 28);color: white;">Sendo</button></a>
					<a href="https://www.sendo.vn/shop/tuong-bong-da-mini/san-pham/"><button class="btn btn-default" style="background: rgb(205, 33, 28);color: white;">Xem Shop</button></a>
				</div>
			</div>
			<div class="row" style="position: relative;top: 17px;">
				<div class="col-md-3">
					<img src="{{ asset('image/lazada.jpg') }}" alt="" style="height: 64px;width: 64px;border-radius: 50px;">
				</div>
				<div class="col-md-9">
					<p style="font-weight: 400;font-size: 2rem;color: rgba(0,0,0,.87);">Anh Hoàng</p>
					<a href="https://www.lazada.vn/tuong-cau-thu-bong-da-mini/?langFlag=vi&q=All-Products&from=wangpu&pageTypeId=2"><button class="btn btn-default" style="background: #404040;color: white;">Lazada</button></a>
					<a href="https://www.lazada.vn/shop/tuong-cau-thu-bong-da-mini/?langFlag=vi&path=profile.htm&pageTypeId=3"><button class="btn btn-default" style="background: #404040;color: white;">Xem Shop</button></a>
				</div>
			</div>
		</div>
		<br>
		<div class="container" style="background: white;">
			<div class="row">
				<div class="col-md-12">
		            <h4 class="description">Mô Tả Sản phẩm</h4>
		          	<p>{!! $product->mieuta !!}</p>
		            <hr>
		          	<h4 class="description">Xuất Xứ</h4>
			        <p>
			           	- Trung Quốc
			           	<br>
			           	- Được những nghệ nhân hàng đầu Trung Quốc thiết kế cũng như sản xuất một cách tinh xảo nhất
			        </p>
		          	<hr>
		          	<h4 class="description">Địa Chỉ</h4>
		            <p>
			          	- Cs1: Hồ Đền Lừ, Quận Hoàng Mai, Hà Nội <br>
			          	- Cs2: 17C, Ngõ 61, Tây sơn, Đống Đa, Hà Nội <br>
			          	- Cs3: Khu Đô Thị SALA, Quận 2, Hồ Chí Minh
		            </p>				 	
				</div>
			</div>
		</div>
		<br>
		<div class="container" style="background: white;">
			<div class="row">
				@if(Auth::check())
					<div class="col-md-12">
 					<h2>Đánh Giá Sản Phẩm</h2>
					     
			        	{{ Form::open(['route' => ['comments', $product->id], 'method' => 'POST']) }}
				            <img src="{{ asset('image/'.Auth::User()->image) }}" class="author-image" style="width: 50px; height: 50px; border-radius:50% ;float: left">
				      		<div class="test" style="padding-left: 56px;padding-top: 10px;">
				      			{{ Form::label('comments', Auth::User()->name ,array('id'=>'test')) }}
				      			<span style="padding-left: 17px;" id="star">
									<i class="fas fa-star color"><span style="display: none;">0</span></i>
									<i class="fas fa-star color"><span style="display: none;">1</span></i>
									<i class="fas fa-star color"><span style="display: none;">2</span></i>
									<i class="fas fa-star color"><span style="display: none;">3</span></i>
									<i class="fas fa-star color"><span style="display: none;">4</span></i>
									<i class=""></i>
								{{ Form::text('chamsao','5',array('id'=>'danhgia','style'=>'border:aliceblue')) }}
								{{-- <label style="display: none;"><input type="text" name="chamsao" value="5" id="danhgia"></label> --}}
				      		</div>
				            <br>
				            {{ Form::textarea('comments',null,array('class'=>'form-control')) }}
				            {{ Form::submit('Add Comment',array('class'=>'btn btn-success btn-lg btn-block')) }}
			            {!! Form::close() !!}

					</div>
				@endif
			</div>
			<br>
			@foreach($product->Comments as $comment)
			<div class="row">
				<div class="col-md-1">
					<img src="{{ asset('image/'.$comment->User->image) }}" class="media-object" style="width:60px;border-radius: 50%;height: 60px">
				</div>
				<div class="col-md-11">
					<div class="jumbotron" id="bodercomment" style="height: 128px">
						<div style="position: relative;right: 35px;bottom: 41px;">
							<b style="font-size: 16px;">{{ $comment->name }}</b>
							<div style="height: 23px">
					  			<div style="display: flex;" id="star">
									<div class="star-eluvation">
										<span class="tyle" style="width: {{ $comment->star>0 ? 100 : 0  }}%">
											<i class="fas fa-star color"></i>
										</span>
									</div>
									<div class="star-eluvation">
										<span class="tyle" style="width: {{ $comment->star>1 ? 100 : 0 }}%">
											<i class="fas fa-star color"></i>
										</span>
									</div>				
									<div class="star-eluvation">
										<span class="tyle" style="width: {{ $comment->star>2 ? 100 : 0 }}%">
											<i class="fas fa-star color"></i>
										</span>
									</div>
									<div class="star-eluvation">
										<span class="tyle" style="width: {{ $comment->star>3 ? 100 : 0 }}%">
											<i class="fas fa-star color"></i>
										</span>
									</div>
									<div class="star-eluvation">
										<span class="tyle" style="width: {{ $comment->star>4 ? 100 : 0 }}%">
											<i class="fas fa-star color"></i>
										</span>
									</div>
								</div>
						  		<div style="display: flex;position: relative;bottom: 16px;color:yellow;">
									<i class="far fa-star color"></i>
									<i class="far fa-star color"></i>
									<i class="far fa-star color"></i>
									<i class="far fa-star color"></i>
									<i class="far fa-star color"></i>
								</div>
							</div>
							<div>
								<span class="comment">Write by •</span>
                      			<span ><a href="" class="comment">{{ $comment->User->name }}</a></span>
                      			<span class="comment"> • {{ $comment->created_date }}</span>
							</div>
							<p style="font-size: 16px;padding-top: 12px;">{{ $comment->comment }}</p>
						</div>
					</div>
				</div>
			</div>
			@endforeach		
		</div>
		<br>
		<div>
			@include('footer')
		</div>

    <!-- Modal-1 -->
		<div id="myModal-1" class="modal fade" role="dialog">
			<div class="modal-dialog" style="padding-left: 117px;margin-left: 327px;">
			    <!-- Modal content-->
			    <div class="modal-content">
			        <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        </div>
				    <div class="modal-body">
				        <img src="{{ asset('image/'.$product->image_main) }}" alt="" style="height: 448px;display: none;" id="image-main-4">
				        <img src="{{ asset('image/'.$product->image_sub1) }}" alt="" style="height: 448px;display: none;" id="image-main-5">
				        <img src="{{ asset('image/'.$product->image_sub2) }}" alt="" style="height: 448px;display: none;" id="image-main-6">
				    </div>
			    </div>
			</div>
		</div>
		{{-- end Madal-1 --}}

	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#tang').click(function(){
		    	var soluong = $('#soluong').val();
		        soluong++;
		        $('#soluong').val(soluong);
		    });

	    	$('#giam').click(function(){
	    		var soluong = $('#soluong').val();
	      		soluong--;
	      		$('#soluong').val(soluong);
	      		if(soluong<=1){
		       		soluong=1;
		       		$('#soluong').val(soluong);
	      		}
	    	});

		    $('#button-image-1').hover(function(){		        
		    	// $('.sizes-image').hide(1);
		    	// $('#image-main-1').slideDown(1);
		    	$("#image-main-1").css("display", "block");
		    	$("#image-main-2").css("display", "none");
		    	$("#image-main-3").css("display", "none");
		    	//$('#image-main-2').slideDown(100);
			  });

		    $('#button-image-1').click(function(){		        
		    	$("#image-main-4").css("display", "block");
		    	$("#image-main-5").css("display", "none");
		    	$("#image-main-6").css("display", "none");
		    });

		    $('#button-image-2').click(function(){		        
		    	$("#image-main-5").css("display", "block");
		    	$("#image-main-4").css("display", "none");
		    	$("#image-main-6").css("display", "none");
		    });

		    $('#button-image-3').click(function(){		        
		    	$("#image-main-6").css("display", "block");
		    	$("#image-main-4").css("display", "none");
		    	$("#image-main-5").css("display", "none");
		    });

		    $('#button-image-2').hover(function(){		        
		    	$("#image-main-1").css("display", "none");
		    	$("#image-main-2").css("display", "block");
		    	$("#image-main-3").css("display", "none");
		    });

		    $('#button-image-3').hover(function(){		        
		    	$("#image-main-1").css("display", "none");
		    	$("#image-main-2").css("display", "none");
		    	$("#image-main-3").css("display", "block");
		    });

			var cart = [
		     	{
		     		name:'',
		     		soluong:0,
		     		image:'',
		     		gia:''
		     	}
		    ];
		    var n=0;
		    var m=0;
		    var check=0;
		    var check2=0;		      
		    var scroll = $('#scroll').children('.sp');

		    for(var i = 0;i<($(scroll).length);i++){
		    	var ob = new Object;
		    	ob.name = $($(scroll[i]).children()[0]).children('.namesp').children('p').text();
		    	ob.image = $($(scroll[i]).children()[0]).children('.img').children().attr('src');
		    	ob.soluong = +$($($(scroll[i]).children()[0]).children('.sl').children()[1]).text();
		    	ob.gia = $($($(scroll[i]).children()[0]).children('.sl').children()[0]).text();
		    	cart[n]=ob;
		    	n=n+1;
		    };

		    $('#themhang').click(function(){
		      // var flyerClone = $('#image-main-1').clone();
		        var flyerClone1 = $('.imageproduct').children();
		        var x = $(flyerClone1).filter(function () { 
				    return this.style.display == 'block' 
				});
				console.log(x);
		        var flyerClone = $(x).clone();
		        // var gotoX1 = $(x).offset().left;
		        // console.log(gotoX1);
		        // var gotoY2 = $(x).offset().top;
		        // console.log(gotoY2);
			    $(flyerClone).css({
			        "position": "absolute",
			        "top": '45px',
			        "left": '454px',
			        "border-radius":"50%",
			        "height":"50px",
			        "width":"50px",
			        "opacity": "1",
			        "z-index": "1000"
			    }).appendTo('body');
		        var gotoX = $('.giohang2').offset().left;
			    var gotoY = $('.giohang2').offset().top;
			    $(flyerClone).animate({
			         opacity: '0.4',
			         left: gotoX,
			         top: gotoY
			        // width: '$(flyingTo).width()'+'px',
			        // height: '$(flyingTo).height()'+'px'
			    });
			    var count = $("#count").text();
			    setTimeout(function(){ 
			    	$(flyerClone).remove();
			    }, 400);

			    var parent = $('#tensp').text();
			    var item = new Object();

			    cart.forEach(function(sanpham){
			    	if(sanpham.name==parent)
			    	{
			    		console.log('acbc');
			    		sanpham.soluong = sanpham.soluong+$('#soluong').val();
			    		check=1;
			    		for(var i = 0;i<($('#scroll').children('.sp').length);i++){
							if($($($($('#scroll').children()[i]).children()[0]).children()[1]).children('p').text()==parent)
							{
								var sl = +$($($($($('#scroll').children()[i]).children()[0]).children()[2]).children()[1]).text()+(+$('#soluong').val())
								$($($($($('#scroll').children()[i]).children()[0]).children()[2]).children()[1]).html(sl);
							} 					    	
						};			    	
			    	};			    	
			    });
			    
		    	if(check==0)
			    	{
			    		count++;
			    		$("#count").text(count);
			    		console.log('tesst');
			    		item.image = $('#image-main-1').attr('src');
			    		// var src = item.image.attr('src');
					    item.name = parent;
					    item.soluong = $('#soluong').val();
					    item.gia = $('#giasp').text();
					    cart[n] = item;
					    // console.log(item.image.attr('src'));
					    $("#scroll").append('<div class="sp"><div class="row">'+'<div class="col-md-2">'+'<img src='+item.image+' alt="" style ="height:30px;width:30px;">'+'</div>'+'<div class="col-md-5 namesp"><p style="font-size:12px">'+item.name+'</p></div><div class="col-md-5"><span style="font-size:12px">'+item.gia+'x'+'</span><span style="font-size:12px">'+item.soluong+'</span><p class="delete" style="cursor: pointer;">Xóa</p></div>'+'</div>'+'<hr style="margin-top: 4px;"></div>');
					    
					    cart[n].name = parent;
					    // cart[n].image = image;
					    cart[n].soluong = 1;
					    cart[n].gia = '65000';
					    n=n+1;
			    	}

			    $.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.post("http://localhost/E-commerce/public/additems", {'name': parent,'soluong':$('#soluong').val()},function(data) {
					console.log(data)
				});
				check=0;
		    });

		    $("#scroll").on("click", "p.delete", function(){
		    	var count = $("#count").text();
				count--;
			   	$("#count").text(count);
				var xoa = $(this).parent().parent().parent();
				$(xoa).remove();				
				var name = $(this).parent().parent().children('.namesp').children().text();
				
				for(var i = 0;i<($(scroll).length);i++){
					if(($($(scroll[i]).children()[0]).children('.namesp').children('p').text())==name)
						{
							$($(scroll[i]).children()[0]).children('.namesp').children('p').html('');
						};
				}
				cart.forEach(function(sanpham){
			    	if(sanpham.name==name)
			    	{
			    		sanpham.name='';
			    	}
			    });

			    $.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
					});
				$.post("http://localhost/E-commerce/public/deletecart", {'name': name},function(data) {    
				});
			});

		    $($("#star").children()).click(function(){
		    	var mang = $("#star").children();
		    	var a = $(this).children().text();
		    	var c =Number((a));
		  		var mang2= [mang[0],mang[1],mang[2],mang[3],mang[4]];
		  		$("#danhgia").attr("value", c+1);
			 	mang2.forEach(function(item){
			 		// console.log(item);
                	var m = $(item).children().text();

                	if(Number(m)>c){
                		$(item).removeClass("fas fa-star color");
                		$(item).addClass("far fa-star color");
                	}
                	else{
                		$(item).removeClass("far fa-star color");
                		$(item).addClass("fas fa-star color");                		
                		$(item).css("color","#ffce3d");
                	}
				});
		    });
		    $(".login-to-buy").click(function(){
		    	$("#myModal-2").modal('show');
		    })
		});
	</script>
@stop