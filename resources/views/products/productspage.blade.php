@foreach($products as $product)
	<div class="col-md-3" id="khung">
		<div class="items">
			<div class="image-item">
				<img src="{{ asset('image/'.$product->image_main) }}" alt="" class="size-image avataritem" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">
				@if(Auth::check())
				 	@if(Auth::User()->can('update',$product))
						<a href="{{ route('products.edit', $product->id) }}" id="edit"><i class="far fa-edit edit"></i></a>
					@endif
				@endif
			</div>
			<div class="title-item">
				<p style="cursor: pointer;"><a href="{{ route('show_product', $product->id) }}">{{ $product->name }}</a></p>
			</div>
			<div class="price-item">
				<p style="float: left;">{{ $product->price/1000 }}.000 vnđ</p>
					@if( $product->stars[0] ==0)
						<div style="padding-left: 71px;">
							<span style="border-left: solid 1px #000;padding-left: 2px;">Chưa Có Đánh Giá</span>
						</div>
					@else
				    <div style="padding-left: 88px">
			  			<div style="display: flex;" id="star">
							<div class="star-eluvation">
								<span class="tyle" style="width: {{ $product->stars[0] }}%">
									<i class="fas fa-star color"></i>
								</span>
							</div>
							<div class="star-eluvation">
								<span class="tyle" style="width: {{ $product->stars[1] }}%">
									<i class="fas fa-star color"></i>
								</span>
							</div>				
							<div class="star-eluvation">
								<span class="tyle" style="width: {{ $product->stars[2] }}%">
									<i class="fas fa-star color"></i>
								</span>
							</div>
							<div class="star-eluvation">
								<span class="tyle" style="width: {{ $product->stars[3] }}%">
									<i class="fas fa-star color"></i>
								</span>
							</div>
							<div class="star-eluvation">
								<span class="tyle" style="width: {{ $product->stars[4] }}%">
									<i class="fas fa-star color"></i>
								</span>
							</div>
						</div>
				  		<div style="display: flex;position: relative;bottom: 16px;color:yellow;">
							<i class="far fa-star color"></i>
							<i class="far fa-star color"></i>
							<i class="far fa-star color"></i>
							<i class="far fa-star color"></i>
							<i class="far fa-star color"></i>
						</div>
					</div>
				@endif
			</div>
			<div class="giohang">
				<button class="btn btn-primary " style="margin-top: -3px;margin-left: 32px;" id="themhang">Thêm Vào Giỏ Hàng</button>
			</div>
		</div>
	</div>
@endforeach	